import http, { Server } from 'http';

import ExpressApplication from 'src/v1/app/ExpressApplication';
import SocketIO from 'src/v1/common/class/SocketIO';
import prismaDatabase from 'src/v1/common/instances/prisma';
import redisServer from 'src/v1/common/instances/redis-client';
import { EXPRESS_PORT } from 'src/v1/env';

class ExpressServer {
  private server: Server;
  private expressApplication: ExpressApplication;
  private socketServer: SocketIO;

  constructor() {
    this.expressApplication = new ExpressApplication();
    this.server = http.createServer(this.expressApplication.getApp());
    this.socketServer = new SocketIO(this.server);
  }

  public listen = () => {
    this.server.listen(EXPRESS_PORT, () => {
      console.log(`Server is running on http://localhost:${EXPRESS_PORT}`);
    });
  };

  public start = async () => {
    try {
      await prismaDatabase.connect();
      await redisServer.connect();
      await redisServer.init();
      this.expressApplication.init();
      this.socketServer.init();
      this.socketServer.listen();
      this.listen();
    } catch (error) {
      console.log((error as Error).message);
    }
  };
}

export default ExpressServer;
