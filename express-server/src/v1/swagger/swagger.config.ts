import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const swaggerJSDocOptions: swaggerJSDoc.Options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Task Management API',
      version: '1.0.0'
    },
    components: {
      securitySchemes: {
        Authentication: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
          description:
            'Enter your access token below to authenticate your requests'
        }
      }
    }
  },
  apis: [`./**/*.{ts,yaml}`]
};

export const swaggerSpec = swaggerJSDoc(swaggerJSDocOptions);
export const swaggerOptions: swaggerUi.SwaggerOptions = {
  defaultModelsExpandDepth: -1,
  plugins: [
    () => {
      return {
        wrapComponents: {
          curl: () => () => null
        }
      };
    }
  ]
};
