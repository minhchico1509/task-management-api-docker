import swaggerUi from 'swagger-ui-express';

import BaseRouter from 'src/v1/common/class/BaseRouter';
import { swaggerOptions, swaggerSpec } from 'src/v1/swagger/swagger.config';

class SwaggerRouter extends BaseRouter {
  constructor() {
    super();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.use(
      '/',
      swaggerUi.serve,
      swaggerUi.setup(swaggerSpec, {
        swaggerOptions,
        customSiteTitle: 'Task Management API Documentation'
      })
    );
  }
}

export default SwaggerRouter;
