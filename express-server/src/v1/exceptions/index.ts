import { NextFunction } from 'express';

import { EHttpStatusCodes } from 'src/v1/common/constants/enum';

export class HttpException extends Error {
  statusCode: EHttpStatusCodes;
  errorDetails: any[] = [];

  constructor(
    statusCode: EHttpStatusCodes,
    message: string,
    errorDetails?: any[]
  ) {
    super(message);
    this.statusCode = statusCode;
    if (errorDetails) {
      this.errorDetails = errorDetails;
    }
  }
}
export class InternalServerErrorException extends HttpException {
  constructor(message: string = 'Internal server error!') {
    super(EHttpStatusCodes.INTERNAL_SERVER_ERROR, message);
  }
}

export class NotFoundException extends HttpException {
  constructor(message: string = 'Not found!', errors?: any[]) {
    super(EHttpStatusCodes.NOT_FOUND, message, errors);
  }
}

export class BadRequestException extends HttpException {
  constructor(message = 'Bad request', errors?: any[]) {
    super(EHttpStatusCodes.BAD_REQUEST, message, errors);
  }
}

export class UnauthorizedException extends HttpException {
  constructor(message = 'Unauthorized', errors?: any[]) {
    super(EHttpStatusCodes.UNAUTHORIZED, message, errors);
  }
}

export const handleError = (next: NextFunction, error: any) => {
  const customErrorInstanceList = [
    BadRequestException,
    NotFoundException,
    UnauthorizedException
  ];

  if (
    customErrorInstanceList.some(
      (errorInstance) => error instanceof errorInstance
    )
  ) {
    next(error);
  } else {
    console.log('Error from server: ', error.message);
    next(new InternalServerErrorException());
  }
};
