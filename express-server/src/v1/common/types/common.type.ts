import { NextFunction, Request, Response } from 'express';

export type TRouterHandler<B = object, P = object, Q = object> = (
  req: Request<P, object, B, Q>,
  res: Response,
  next: NextFunction
) => Promise<any>;

export interface IJWTPayload {
  id: string;
  email: string;
}

export interface IAccessJWTPayload extends IJWTPayload {}
export interface IRefreshJWTPayload extends IJWTPayload {}
