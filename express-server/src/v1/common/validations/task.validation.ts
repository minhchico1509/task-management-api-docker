import dayjs from 'dayjs';

import { db } from 'src/v1/common/instances/prisma';
import {
  BadRequestException,
  NotFoundException,
  UnauthorizedException
} from 'src/v1/exceptions';
import { ERecurringType } from 'src/v1/modules/task/constants/task.enum';

export const checkPerformersExistanceInRoom = async (
  performers: string[],
  roomId: string
) => {
  const users = await db.roomMember.findMany({
    where: {
      roomId,
      userId: {
        in: performers
      }
    }
  });
  if (users.length !== performers.length) {
    throw new BadRequestException('Some performers do not exist in the room');
  }
};

export const checkTaskExistance = async (taskId: string) => {
  const task = await db.task.findUnique({
    where: { id: taskId },
    select: { roomId: true, startDate: true, dueDate: true }
  });

  if (!task) {
    throw new NotFoundException('Task does not exist');
  }
  return {
    roomId: task.roomId,
    startDate: task.startDate,
    dueDate: task.dueDate
  };
};

export const checkUserIsPerformerOrAdminOfTask = async (
  taskId: string,
  userId: string
) => {
  const task = await db.task.findFirst({
    where: {
      id: taskId,
      OR: [{ performers: { some: { userId } } }, { assigner: { userId } }]
    },
    select: {
      performers: { where: { userId } },
      assignerId: true
    }
  });

  if (!task) {
    throw new UnauthorizedException(
      'You are not performer or admin of this task'
    );
  }
  return {
    memberId: task.performers[0]?.id || task.assignerId
  };
};

export const checkValidRecurringTask = (
  startRecurringDate: Date,
  endRecurringDate: Date,
  startDate: Date,
  dueDate: Date,
  recurringType: ERecurringType
) => {
  if (recurringType === ERecurringType.DAILY) {
    //Nếu là daily task thì thời điểm bắt đầu và kết thúc công việc phải cùng 1 ngày
    if (!dayjs(dueDate).isSame(dayjs(startDate), 'day')) {
      throw new BadRequestException(
        'Due date of task must be the same day with start date of task'
      );
    }
    //Nếu là daily task thì thời điểm bắt đầu và kết thúc lặp phải lớn hơn 1 ngày
    if (dayjs(endRecurringDate).diff(dayjs(startRecurringDate), 'day') < 1) {
      throw new BadRequestException(
        'Start recurring date and end recurring date must be different more than 1 day'
      );
    }
  } else {
    //Nếu là weekly task thì thời điểm bắt đầu và kết thúc lặp phải lớn hơn 7 ngày
    if (dayjs(endRecurringDate).diff(dayjs(startRecurringDate), 'day') < 7) {
      throw new BadRequestException(
        'Start recurring date and end recurring date must be different more than 7 days'
      );
    }
  }
};
