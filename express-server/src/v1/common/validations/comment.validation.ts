import { db } from 'src/v1/common/instances/prisma';
import { BadRequestException, NotFoundException } from 'src/v1/exceptions';

export const checkCommentExistance = async (commentId: string) => {
  const comment = await db.comment.findUnique({
    where: { id: commentId }
  });

  if (!comment) {
    throw new NotFoundException('Comment not found');
  }
};

export const checkIfUserIsOwnerOfComment = async (
  userId: string,
  commentId: string
) => {
  const comment = await db.comment.findUnique({
    where: { id: commentId },
    select: { commentator: { select: { userId: true } } }
  });

  if (comment?.commentator.userId !== userId) {
    throw new BadRequestException('You are not owner of this comment');
  }
};
