import { db } from 'src/v1/common/instances/prisma';
import {
  BadRequestException,
  NotFoundException,
  UnauthorizedException
} from 'src/v1/exceptions';

export const checkRoomExistance = async (roomId: string) => {
  const room = await db.room.findUnique({
    where: { id: roomId }
  });
  if (!room) {
    throw new NotFoundException('Room not found');
  }
};

export const checkUserIsMemberOfRoom = async (
  userId: string,
  roomId: string,
  mustTrue: boolean
) => {
  const roomMember = await db.roomMember.findFirst({
    where: { roomId, userId }
  });
  if (!mustTrue && roomMember) {
    throw new BadRequestException('You are already a member of this room');
  }
  if (mustTrue && !roomMember) {
    throw new UnauthorizedException('You are not a member of this room');
  }
};

export const checkUserIsAdminOfRoom = async (
  userId: string,
  roomId: string
) => {
  const room = await db.room.findFirst({
    where: { id: roomId, roomMembers: { some: { userId, role: 'ADMIN' } } }
  });
  if (!room) {
    throw new UnauthorizedException('You are not an admin of this room');
  }
};
