import { db } from 'src/v1/common/instances/prisma';
import { NotFoundException } from 'src/v1/exceptions';

export const checkSubTaskExistance = async (subTaskId: string) => {
  const subTasks = await db.subTask.findUnique({
    where: { id: subTaskId },
    select: {
      taskId: true,
      state: true
    }
  });

  if (!subTasks) {
    throw new NotFoundException('Sub task not found');
  }

  return {
    taskId: subTasks?.taskId || '',
    subTaskStatus: subTasks?.state || ''
  };
};
