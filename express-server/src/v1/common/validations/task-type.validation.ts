import { db } from 'src/v1/common/instances/prisma';
import { BadRequestException, NotFoundException } from 'src/v1/exceptions';

export const checkTaskTypeExistanceById = async (taskTypeId: string) => {
  const taskType = await db.taskType.findUnique({
    where: { id: taskTypeId },
    select: { roomId: true }
  });

  if (!taskType) {
    throw new NotFoundException('Task type not found');
  }
  return {
    roomId: taskType.roomId
  };
};

export const checkIsTaskTypeNameExistInRoom = async (
  roomId: string,
  name: string,
  mustExist: boolean
) => {
  const taskType = await db.taskType.findFirst({
    where: { name, roomId }
  });

  if (mustExist && !taskType) {
    throw new NotFoundException('Task type not found');
  }

  if (!mustExist && taskType) {
    throw new BadRequestException('Task type name already exists');
  }
  return {
    taskTypeId: taskType?.id
  };
};
