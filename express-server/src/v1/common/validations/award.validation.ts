import { db } from 'src/v1/common/instances/prisma';
import { BadRequestException, NotFoundException } from 'src/v1/exceptions';

export const checkAwardExistance = async (awardId: string) => {
  const award = await db.award.findUnique({
    where: { id: awardId },
    select: { roomId: true }
  });

  if (!award) {
    throw new NotFoundException('Award not found');
  }

  return {
    roomId: award.roomId
  };
};

export const checkUserIsEligibleToRetrieveAward = async (
  userId: string,
  awardId: string
) => {
  const award = await db.award.findUnique({
    where: { id: awardId },
    select: { minScore: true, roomId: true }
  });

  const member = await db.roomMember.findFirst({
    where: { userId, roomId: award?.roomId },
    select: { accumulatedScore: true, id: true }
  });

  if ((member?.accumulatedScore || 0) < (award?.minScore || 0)) {
    throw new BadRequestException(
      'You do not have enough score to retrieve this award'
    );
  }
  return {
    memberId: member?.id
  };
};
