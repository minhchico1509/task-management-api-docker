import { NextFunction, Request, Response } from 'express';

import { HttpException } from 'src/v1/exceptions';

const errorResponseMiddleware = (
  error: HttpException,
  _request: Request,
  response: Response,
  _next: NextFunction
) => {
  const { statusCode, message, errorDetails } = error;
  response.status(statusCode).json({
    statusCode,
    message,
    errorDetails
  });
};

export default errorResponseMiddleware;
