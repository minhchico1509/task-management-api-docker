import { NextFunction, Request, Response } from 'express';

import JWT from 'src/v1/common/class/JWT';
import { EHeaderKey } from 'src/v1/common/constants/enum';
import { db } from 'src/v1/common/instances/prisma';
import { IAccessJWTPayload } from 'src/v1/common/types/common.type';
import { ACCESS_TOKEN_SECRET } from 'src/v1/env';
import { UnauthorizedException } from 'src/v1/exceptions';

const userAuthentication = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = (req.headers[EHeaderKey.AUTHORIZATION] as string)?.split(
    ' '
  )[1];

  if (!token) {
    return next(new UnauthorizedException('Token is required!'));
  }

  const decodedToken = await new JWT(
    ACCESS_TOKEN_SECRET
  ).verify<IAccessJWTPayload>(token);

  if (!decodedToken) {
    return next(new UnauthorizedException('Invalid token!'));
  }

  const user = await db.user.findUnique({
    where: { id: decodedToken.id }
  });
  if (!user) {
    return next(new UnauthorizedException('User not found!'));
  }

  req.headers[EHeaderKey.USER_ID] = user.id;

  next();
};

export default userAuthentication;
