import dayjs from 'dayjs';
import { NextFunction, Request, Response } from 'express';

const logger = (req: Request, res: Response, next: NextFunction) => {
  const method = req.method;
  const url = req.url;

  res.on('finish', () => {
    const statusCode = res.statusCode;
    const requestStatus = statusCode < 400 ? 'SUCCESS' : 'FAILED';
    const time = dayjs().format('DD/MM/YYYY HH:mm:ss');
    const loggerMessage = `${time}: ${method} ${url} - ${statusCode}: ${requestStatus}\n`;
    if (url.startsWith('/api/v1')) {
      console.log(loggerMessage);
    }
  });

  next();
};

export default logger;
