import { Exclude, Expose } from 'class-transformer';

@Exclude()
class RoomResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  name!: string;

  @Expose()
  description!: string;

  @Expose()
  totalMembers?: number;
}

export default RoomResponseDTO;
