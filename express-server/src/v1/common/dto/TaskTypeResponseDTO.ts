import { Exclude, Expose } from 'class-transformer';

@Exclude()
class TaskTypeResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  name!: string;
}

export default TaskTypeResponseDTO;
