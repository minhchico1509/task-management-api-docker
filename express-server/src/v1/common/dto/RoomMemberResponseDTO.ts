import { RoomRole } from '@prisma/client';
import { Exclude, Expose, Type } from 'class-transformer';

import UserResponseDTO from 'src/v1/common/dto/UserResponseDTO';

@Exclude()
class RoomMemberResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  @Type(() => UserResponseDTO)
  profile!: UserResponseDTO;

  @Expose()
  joinedAt!: Date;

  @Expose()
  role!: RoomRole;

  @Expose()
  accumulatedScore!: number;
}

export default RoomMemberResponseDTO;
