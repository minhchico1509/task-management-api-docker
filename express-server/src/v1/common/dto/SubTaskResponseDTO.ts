import { SubTaskState } from '@prisma/client';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
class SubTaskResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  description!: string;

  @Expose()
  state!: SubTaskState;
}

export default SubTaskResponseDTO;
