import { TaskPriority, TaskStatus } from '@prisma/client';
import { Exclude, Expose, Type } from 'class-transformer';

import RoomMemberResponseDTO from 'src/v1/common/dto/RoomMemberResponseDTO';
import SubTaskResponseDTO from 'src/v1/common/dto/SubTaskResponseDTO';
import TaskTypeResponseDTO from 'src/v1/common/dto/TaskTypeResponseDTO';

@Exclude()
class TaskResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  title!: string;

  @Expose()
  description!: string;

  @Expose()
  priority!: TaskPriority;

  @Expose()
  status!: TaskStatus;

  @Expose()
  startDate!: Date;

  @Expose()
  dueDate!: Date;

  @Expose()
  score!: number;

  @Expose()
  isRecurring!: boolean;

  @Expose()
  @Type(() => RoomMemberResponseDTO)
  assigner!: RoomMemberResponseDTO;

  @Expose()
  @Type(() => RoomMemberResponseDTO)
  performers!: RoomMemberResponseDTO[];

  @Expose()
  @Type(() => SubTaskResponseDTO)
  subTasks!: SubTaskResponseDTO[];

  @Expose()
  @Type(() => TaskTypeResponseDTO)
  taskType!: TaskTypeResponseDTO;

  @Expose()
  createdAt!: Date;

  @Expose()
  updatedAt!: Date;
}

export default TaskResponseDTO;
