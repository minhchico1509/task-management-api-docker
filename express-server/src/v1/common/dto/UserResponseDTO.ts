import { Exclude, Expose } from 'class-transformer';

@Exclude()
class UserResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  fullName!: string;

  @Expose()
  email!: string;
}

export default UserResponseDTO;
