import { Exclude, Expose, Type } from 'class-transformer';

import RoomMemberResponseDTO from 'src/v1/common/dto/RoomMemberResponseDTO';

@Exclude()
class CommentResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  content!: string;

  @Expose()
  createdAt!: Date;

  @Expose()
  updatedAt!: Date;

  @Expose()
  @Type(() => RoomMemberResponseDTO)
  commentator!: RoomMemberResponseDTO;
}

export default CommentResponseDTO;
