import { Exclude, Expose, Type } from 'class-transformer';

import RoomMemberResponseDTO from 'src/v1/common/dto/RoomMemberResponseDTO';

@Exclude()
class AwardResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  name!: string;

  @Expose()
  description!: string;

  @Expose()
  minScore!: number;

  @Expose()
  @Type(() => RoomMemberResponseDTO)
  awardPrsenter!: RoomMemberResponseDTO;
}

export default AwardResponseDTO;
