import { instrument } from '@socket.io/admin-ui';
import { Server as HTTPServer } from 'http';
import { Server as SocketServer } from 'socket.io';

import JWT from 'src/v1/common/class/JWT';
import { EHeaderKey } from 'src/v1/common/constants/enum';
import { IAccessJWTPayload } from 'src/v1/common/types/common.type';
import { ACCESS_TOKEN_SECRET } from 'src/v1/env';

class SocketIO {
  private io: SocketServer;

  constructor(httpServer: HTTPServer) {
    this.io = new SocketServer(httpServer, {
      cors: {
        origin: ['https://admin.socket.io', 'http://localhost:3000'],
        credentials: true
      }
    });
  }

  public init = () => {
    instrument(this.io, {
      auth: false,
      mode: 'development'
    });
  };

  public initMiddleware = () => {
    return this.io.use(async (socket, next) => {
      const token = socket.handshake.headers.authorization?.split(' ')[1];
      if (!token) {
        return next(new Error('Token is required!'));
      }
      const decodedToken = await new JWT(
        ACCESS_TOKEN_SECRET
      ).verify<IAccessJWTPayload>(token);
      if (!decodedToken) {
        return next(new Error('Invalid token!'));
      }
      socket.handshake.headers[EHeaderKey.USER_ID] = decodedToken.id;
      next();
    });
  };

  public listen = () => {
    this.initMiddleware().on('connection', (socket) => {
      const user = {
        userId: socket.handshake.headers[EHeaderKey.USER_ID],
        socketId: socket.id
      };
      console.log('A user connected:', user);

      socket.on('disconnect', () => {
        console.log(`User ${socket.id} disconnected`);
      });

      socket.on('CHAT_MESSAGE', (message) => {
        const { content, to } = message;
        socket.to(to).emit('NEW_MESSAGE', content);
      });
    });
  };
}

export default SocketIO;
