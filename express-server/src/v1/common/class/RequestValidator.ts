import {
  ClassConstructor,
  instanceToPlain,
  plainToInstance
} from 'class-transformer';
import { validate } from 'class-validator';
import { Request } from 'express';

import { BadRequestException } from 'src/v1/exceptions';

class RequestValidator {
  static validateJSON = async (
    classInstance: ClassConstructor<object>,
    req: Request<object, object, object, object>,
    type: 'BODY' | 'QUERY' = 'BODY'
  ) => {
    const data = type === 'BODY' ? req.body : req.query;
    const convertedInstance = plainToInstance(classInstance, data);
    const validationErrors = await validate(convertedInstance);

    if (validationErrors.length > 0) {
      const errorDetails = validationErrors.map((error) => ({
        field: error.property,
        error: Object.values(error.constraints as object).reverse()[0]
      }));
      const message = 'Request validation failed!';
      throw new BadRequestException(message, errorDetails);
    }

    if (type === 'BODY') {
      req.body = instanceToPlain(convertedInstance);
    } else {
      req.query = instanceToPlain(convertedInstance);
    }
  };
}

export default RequestValidator;
