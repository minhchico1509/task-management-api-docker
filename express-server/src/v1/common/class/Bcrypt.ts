import * as bcrypt from 'bcrypt';

class Bcrypt {
  static encrypt = async (value: string) => {
    const salt = await bcrypt.genSalt(10);
    const hashedValue = await bcrypt.hash(value, salt);
    return hashedValue;
  };

  static compareValue = async (value: string, hashedValue: string) => {
    const isMatch = await bcrypt.compare(value, hashedValue);
    return isMatch;
  };
}

export default Bcrypt;
