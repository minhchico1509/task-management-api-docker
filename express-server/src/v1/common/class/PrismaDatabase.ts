import { PrismaClient } from '@prisma/client';

class PrismaDatabase {
  private db: PrismaClient;

  constructor() {
    this.db = new PrismaClient();
  }

  public getDb = () => {
    return this.db;
  };

  public connect = async () => {
    try {
      await this.db.$connect();
      console.log('✅ Connect to database successfully!');
    } catch (error) {
      this.db.$disconnect();
      throw new Error(
        `❌ Database connection failed (${(error as Error).message})`
      );
    }
  };
}

export default PrismaDatabase;
