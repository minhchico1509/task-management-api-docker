import { decodeJwt, jwtVerify, SignJWT } from 'jose';

class JWT {
  private key: Uint8Array;

  constructor(secretKey: string = '') {
    this.key = new TextEncoder().encode(secretKey);
  }

  public sign = async (payload: any, expirationTime: string) => {
    const token = await new SignJWT(payload)
      .setProtectedHeader({ alg: 'HS256' })
      .setIssuedAt()
      .setExpirationTime(expirationTime)
      .sign(this.key);

    const { exp } = decodeJwt(token);
    return {
      token,
      exp
    };
  };

  public verify = async <T>(input: string): Promise<T | null> => {
    try {
      const { payload } = await jwtVerify(input, this.key, {
        algorithms: ['HS256']
      });
      return payload as T;
    } catch (error) {
      return null;
    }
  };
}

export default JWT;
