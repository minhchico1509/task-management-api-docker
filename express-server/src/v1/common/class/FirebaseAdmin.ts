import admin, { ServiceAccount } from 'firebase-admin';

import { FIREBASE_DATABASE_URL, FIREBASE_STORAGE_BUCKET } from 'src/v1/env';
import serviceAccount from 'src/v1/json/serviceAccountKey.json';

class FirebaseAdmin {
  constructor() {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount as ServiceAccount),
      storageBucket: FIREBASE_STORAGE_BUCKET,
      databaseURL: FIREBASE_DATABASE_URL
    });
  }

  public getBucketStorage = () => {
    return admin.storage().bucket();
  };

  public getMessaging = () => {
    return admin.messaging();
  };

  public getAuth = () => {
    return admin.auth();
  };

  public getDatabase = () => {
    return admin.database();
  };
}

export default FirebaseAdmin;
