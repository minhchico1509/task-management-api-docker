import { Redis } from 'ioredis';

class RedisServer {
  private redisClient: Redis;

  public getRedisClient = () => {
    return this.redisClient;
  };

  constructor(host: string, port: number, password: string) {
    this.redisClient = new Redis({
      host,
      port,
      password
    });
  }

  public connect = async () => {
    this.redisClient.on('error', () => {
      this.redisClient.disconnect();
    });

    this.redisClient.on('ready', () => {
      console.log('✅ Connected to Redis successfully');
    });

    try {
      const info = await this.redisClient.info();
    } catch (error) {
      throw new Error(
        `❌ Redis connection failed (${(error as Error).message})`
      );
    }
  };

  public init = async () => {
    await this.redisClient.config('SET', 'notify-keyspace-events', 'KEA');
  };
}

export default RedisServer;
