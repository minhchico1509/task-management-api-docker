import firebaseAdmin from 'src/v1/common/instances/firebase-admin';
import { redisClient } from 'src/v1/common/instances/redis-client';

export const sendNotification = async (
  deviceToken: string[],
  title: string,
  body: string,
  data?: any
) => {
  try {
    const sentNotification = await firebaseAdmin
      .getMessaging()
      .sendEachForMulticast({
        tokens: deviceToken,
        notification: {
          title,
          body
        },
        data
      });
  } catch (error) {}
};

export const getDeviceTokensFromUserIds = async (userIds: string[]) => {
  const pipeline = redisClient.pipeline();
  userIds.forEach((userId) => {
    pipeline.smembers(`device_token:${userId}`);
  });
  const executedResult = await pipeline.exec();
  const deviceTokens =
    executedResult?.reduce((prev: string[], [error, result]: any[]) => {
      return [...prev, ...(result || [])];
    }, []) || [];
  return Array.from(new Set(deviceTokens));
};
