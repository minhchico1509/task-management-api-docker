import { OAuth2Client } from 'google-auth-library';

import { GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET } from 'src/v1/env';

const oAuth2Client = new OAuth2Client({
  clientId: GOOGLE_CLIENT_ID,
  clientSecret: GOOGLE_CLIENT_SECRET,
  redirectUri: 'postmessage'
});

export default oAuth2Client;
