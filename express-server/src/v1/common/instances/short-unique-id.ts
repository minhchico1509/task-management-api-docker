import ShortUniqueId from 'short-unique-id';

const shortUniqueId = new ShortUniqueId({ length: 7 });
export default shortUniqueId;
