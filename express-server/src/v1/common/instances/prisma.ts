import PrismaDatabase from 'src/v1/common/class/PrismaDatabase';

const prismaDatabase = new PrismaDatabase();

export const db = prismaDatabase.getDb();
export default prismaDatabase;
