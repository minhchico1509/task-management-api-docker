import RedisServer from 'src/v1/common/class/RedisServer';
import { REDIS_HOST, REDIS_PASSWORD, REDIS_PORT } from 'src/v1/env';

const redisHost = REDIS_HOST || 'localhost';
const redisPort = Number(REDIS_PORT) || 6379;
const redisPassword = REDIS_PASSWORD || '';

const redisServer = new RedisServer(redisHost, redisPort, redisPassword);

export const redisClient = redisServer.getRedisClient();
export default redisServer;
