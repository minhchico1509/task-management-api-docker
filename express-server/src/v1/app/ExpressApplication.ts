import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, { Application } from 'express';

import AppRouter from 'src/v1/app/AppRouter';
import errorResponseMiddleware from 'src/v1/common/middlewares/error-response';
import logger from 'src/v1/common/middlewares/logger';
import limiter from 'src/v1/common/middlewares/rate-limit';
import SwaggerRouter from 'src/v1/swagger/swagger.route';

class ExpressApplication {
  private app: Application;

  constructor() {
    this.app = express();
  }

  public getApp = () => this.app;

  public init = () => {
    this.initializeMiddlewares();
    this.initializeAppRouter();
    this.initializeErrorHandling();
  };

  public initializeMiddlewares = () => {
    this.app.use(limiter);
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cookieParser());
    this.app.use(logger);
  };

  public initializeAppRouter = () => {
    this.app.use('/api/v1', new AppRouter().getRouter());
    this.app.use('/', new SwaggerRouter().getRouter());
  };

  public initializeErrorHandling() {
    this.app.use(errorResponseMiddleware);
  }
}

export default ExpressApplication;
