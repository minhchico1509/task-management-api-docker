import BaseRouter from 'src/v1/common/class/BaseRouter';
import userAuthentication from 'src/v1/common/middlewares/user-authentication';
import AuthRouter from 'src/v1/modules/auth/routers/auth.router';
import AwardRouter from 'src/v1/modules/award/routers/award.router';
import CommentRouter from 'src/v1/modules/comment/routers/comment.router';
import RoomRouter from 'src/v1/modules/room/routers/room.router';
import TaskRouter from 'src/v1/modules/task/routers/task.router';
import TaskTypeRouter from 'src/v1/modules/task-type/routers/task-type.router';
import UserRouter from 'src/v1/modules/user/routers/user.router';

class AppRouter extends BaseRouter {
  constructor() {
    super();
    this.initializeAppRouter();
  }

  private initializeAppRouter() {
    this.initializeUnauthorizedRouter();
    this.initializeAuthorizedRouter();
  }

  private initializeAuthorizedRouter() {
    this.router.use(userAuthentication);
    this.router.use('/', new UserRouter().getRouter());
    this.router.use('/', new RoomRouter().getRouter());
    this.router.use('/', new TaskTypeRouter().getRouter());
    this.router.use('/', new TaskRouter().getRouter());
    this.router.use('/', new AwardRouter().getRouter());
    this.router.use('/', new CommentRouter().getRouter());
  }

  public initializeUnauthorizedRouter() {
    this.router.use('/', new AuthRouter().getRouter());
  }
}

export default AppRouter;
