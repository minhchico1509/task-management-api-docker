import { EHttpStatusCodes } from 'src/v1/common/constants/enum';
import { handleError } from 'src/v1/exceptions';
import AuthService from 'src/v1/modules/auth/services/auth.service';
import TLoginHandler from 'src/v1/modules/auth/types/handlers/login.type';
import TSignupHandler from 'src/v1/modules/auth/types/handlers/signup.type';

class AuthController {
  private authService: AuthService;

  constructor() {
    this.authService = new AuthService();
  }

  public signUp: TSignupHandler = async (req, res, next) => {
    try {
      const responseData = await this.authService.signUp(req.body);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public login: TLoginHandler = async (req, res, next) => {
    try {
      const responseData = await this.authService.login(req.body);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default AuthController;
