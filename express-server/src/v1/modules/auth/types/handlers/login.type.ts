import { TRouterHandler } from 'src/v1/common/types/common.type';
import LoginRequestDTO from 'src/v1/modules/auth/dto/login/LoginRequestDTO';

type TLoginHandler = TRouterHandler<LoginRequestDTO>;

export default TLoginHandler;
