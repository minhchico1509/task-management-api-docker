import { TRouterHandler } from 'src/v1/common/types/common.type';
import SignupRequestDTO from 'src/v1/modules/auth/dto/signup/SignupRequestDTO';

type TSignupHandler = TRouterHandler<SignupRequestDTO>;

export default TSignupHandler;
