import LoginRequestDTO from 'src/v1/modules/auth/dto/login/LoginRequestDTO';
import LoginResponseDTO from 'src/v1/modules/auth/dto/login/LoginResponseDTO';
import SignupRequestDTO from 'src/v1/modules/auth/dto/signup/SignupRequestDTO';
import SignupResponseDTO from 'src/v1/modules/auth/dto/signup/SignupResponseDTO';

interface IAuthService {
  signUp: (bodyData: SignupRequestDTO) => Promise<SignupResponseDTO>;
  login: (bodyData: LoginRequestDTO) => Promise<LoginResponseDTO>;
}

export default IAuthService;
