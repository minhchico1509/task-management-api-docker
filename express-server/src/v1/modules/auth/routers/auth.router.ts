import BaseRouter from 'src/v1/common/class/BaseRouter';
import AuthController from 'src/v1/modules/auth/controllers/auth.controller';
import AuthMiddleware from 'src/v1/modules/auth/middlewares/auth.middleware';

class AuthRouter extends BaseRouter {
  private authController: AuthController;
  private authMiddleware: AuthMiddleware;

  constructor() {
    super();
    this.authController = new AuthController();
    this.authMiddleware = new AuthMiddleware();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.post(
      '/auth/signup',
      this.authMiddleware.signup,
      this.authController.signUp
    );
    this.router.post(
      '/auth/login',
      this.authMiddleware.login,
      this.authController.login
    );
  }
}

export default AuthRouter;
