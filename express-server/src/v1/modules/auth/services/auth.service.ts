import { plainToInstance } from 'class-transformer';

import Bcrypt from 'src/v1/common/class/Bcrypt';
import JWT from 'src/v1/common/class/JWT';
import { db } from 'src/v1/common/instances/prisma';
import { IJWTPayload } from 'src/v1/common/types/common.type';
import { ACCESS_TOKEN_SECRET } from 'src/v1/env';
import { BadRequestException } from 'src/v1/exceptions';
import { ETokenExpiry } from 'src/v1/modules/auth/constants';
import LoginRequestDTO from 'src/v1/modules/auth/dto/login/LoginRequestDTO';
import LoginResponseDTO from 'src/v1/modules/auth/dto/login/LoginResponseDTO';
import SignupRequestDTO from 'src/v1/modules/auth/dto/signup/SignupRequestDTO';
import SignupResponseDTO from 'src/v1/modules/auth/dto/signup/SignupResponseDTO';
import IAuthService from 'src/v1/modules/auth/types/services/auth-service.interface';

class AuthService implements IAuthService {
  public signUp = async (bodyData: SignupRequestDTO) => {
    const { email, password, fullName } = bodyData;
    const isEmailExist = !!(await db.user.findFirst({
      where: { email }
    }));
    if (isEmailExist) {
      throw new BadRequestException('Email already exist');
    }

    const hashedPassword = await Bcrypt.encrypt(password);
    const createdUser = await db.user.create({
      data: {
        email,
        password: hashedPassword,
        fullName
      }
    });

    const responseData: SignupResponseDTO = {
      ...createdUser
    };

    return plainToInstance(SignupResponseDTO, responseData);
  };

  public login = async (bodyData: LoginRequestDTO) => {
    const { email, password } = bodyData;
    const user = await db.user.findFirst({
      where: { email }
    });
    if (!user) {
      throw new BadRequestException('Email is not associated with any account');
    }

    const isPasswordMatch = await Bcrypt.compareValue(
      password,
      user.password || ''
    );

    if (!isPasswordMatch) {
      throw new BadRequestException('Wrong password');
    }

    const userPayload: IJWTPayload = {
      id: user.id,
      email: user.email
    };

    const accessTokenPayload = await new JWT(ACCESS_TOKEN_SECRET).sign(
      userPayload,
      ETokenExpiry.ACCESS_TOKEN
    );

    const responseData: LoginResponseDTO = {
      user,
      accessToken: accessTokenPayload.token
    };

    return plainToInstance(LoginResponseDTO, responseData);
  };
}

export default AuthService;
