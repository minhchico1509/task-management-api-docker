import { Exclude, Expose, Type } from 'class-transformer';

import UserResponseDTO from 'src/v1/common/dto/UserResponseDTO';

@Exclude()
class LoginResponseDTO {
  @Expose()
  @Type(() => UserResponseDTO)
  user!: UserResponseDTO;

  @Expose()
  accessToken!: string;
}

export default LoginResponseDTO;
