import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
class LoginRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  email!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  password!: string;
}

export default LoginRequestDTO;
