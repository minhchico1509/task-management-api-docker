import { Exclude, Expose } from 'class-transformer';
import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength
} from 'class-validator';

@Exclude()
class SignupRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  password!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  fullName!: string;
}

export default SignupRequestDTO;
