import { Exclude, Expose } from 'class-transformer';

@Exclude()
class SignupResponseDTO {
  @Expose()
  id!: string;

  @Expose()
  fullName!: string;

  @Expose()
  email!: string;
}

export default SignupResponseDTO;
