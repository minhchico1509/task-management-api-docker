import RequestValidator from 'src/v1/common/class/RequestValidator';
import { handleError } from 'src/v1/exceptions';
import LoginRequestDTO from 'src/v1/modules/auth/dto/login/LoginRequestDTO';
import SignupRequestDTO from 'src/v1/modules/auth/dto/signup/SignupRequestDTO';
import TLoginHandler from 'src/v1/modules/auth/types/handlers/login.type';
import TSignupHandler from 'src/v1/modules/auth/types/handlers/signup.type';

class AuthMiddleware {
  constructor() {}

  public login: TLoginHandler = async (req, res, next) => {
    try {
      await RequestValidator.validateJSON(LoginRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public signup: TSignupHandler = async (req, res, next) => {
    try {
      await RequestValidator.validateJSON(SignupRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default AuthMiddleware;
