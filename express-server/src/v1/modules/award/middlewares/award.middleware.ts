import RequestValidator from 'src/v1/common/class/RequestValidator';
import { EHeaderKey } from 'src/v1/common/constants/enum';
import {
  checkAwardExistance,
  checkUserIsEligibleToRetrieveAward
} from 'src/v1/common/validations/award.validation';
import {
  checkRoomExistance,
  checkUserIsAdminOfRoom,
  checkUserIsMemberOfRoom
} from 'src/v1/common/validations/room.validation';
import { handleError } from 'src/v1/exceptions';
import CreateAwardRequestDTO from 'src/v1/modules/award/dto/create-award/CreateAwardRequestDTO';
import UpdateAwardRequestDTO from 'src/v1/modules/award/dto/update-award/UpdateAwardRequestDTO';
import TCreateAwardHandler from 'src/v1/modules/award/types/handlers/create-award.type';
import TDeleteAwardHandler from 'src/v1/modules/award/types/handlers/delete-award.type';
import TUpdateAwardHandler from 'src/v1/modules/award/types/handlers/edit-award.type';
import TGetAwardsInRoomParams from 'src/v1/modules/award/types/handlers/get-awards-in-room.type';
import TRetrieveAwardHandler from 'src/v1/modules/award/types/handlers/retrieve-award.type';

class AwardMiddleware {
  constructor() {}

  public createAward: TCreateAwardHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkRoomExistance(roomId);
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(CreateAwardRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateAward: TUpdateAwardHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { awardId } = req.params;
      const { roomId } = await checkAwardExistance(awardId);
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(UpdateAwardRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteAward: TDeleteAwardHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { awardId } = req.params;
      const { roomId } = await checkAwardExistance(awardId);
      await checkUserIsAdminOfRoom(userId, roomId);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getAwardsInRoom: TGetAwardsInRoomParams = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkUserIsMemberOfRoom(userId, roomId, true);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public retrieveAward: TRetrieveAwardHandler = async (req, res, next) => {
    try {
      const { awardId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { roomId } = await checkAwardExistance(awardId);
      await checkUserIsMemberOfRoom(userId, roomId, true);
      const { memberId } = await checkUserIsEligibleToRetrieveAward(
        userId,
        awardId
      );
      req.body.memberId = memberId || '';
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default AwardMiddleware;
