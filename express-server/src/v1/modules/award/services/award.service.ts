import { plainToInstance } from 'class-transformer';

import { db } from 'src/v1/common/instances/prisma';
import CreateAwardRequestDTO from 'src/v1/modules/award/dto/create-award/CreateAwardRequestDTO';
import CreateAwardResponseDTO from 'src/v1/modules/award/dto/create-award/CreateAwardResponseDTO';
import DeleteAwardResponseDTO from 'src/v1/modules/award/dto/delete-award/DeleteAwardResponseDTO';
import GetReceivedAwardsResponseDTO from 'src/v1/modules/award/dto/get-received-awards/GetReceivedAwardsResponseDTO';
import GetUnReceivedAwardResponseDTO from 'src/v1/modules/award/dto/get-unreceived-awards/GetUnReceivedAwardResponseDTO';
import RetrieveAwardResponseDTO from 'src/v1/modules/award/dto/retrieve-award/RetrieveAwardResponseDTO';
import UpdateAwardRequestDTO from 'src/v1/modules/award/dto/update-award/UpdateAwardRequestDTO';
import UpdateAwardResponseDTO from 'src/v1/modules/award/dto/update-award/UpdateAwardResponseDTO';
import { formatAwardResponse } from 'src/v1/modules/award/utils/award-response.util';

class AwardService {
  constructor() {}

  public createAward = async (
    roomId: string,
    userId: string,
    awardData: CreateAwardRequestDTO
  ) => {
    const { name, description, minScore } = awardData;
    const presenterMember = await db.roomMember.findFirst({
      where: { userId, roomId },
      select: { id: true }
    });
    const createdAward = await db.award.create({
      data: {
        name,
        description,
        minScore,
        awardPresenterId: presenterMember?.id || '',
        roomId
      },
      include: { awardPresenter: { include: { user: true } } }
    });

    const responseData = {
      message: 'Award created successfully',
      createdAward: formatAwardResponse(createdAward)
    } as CreateAwardResponseDTO;

    return plainToInstance(CreateAwardResponseDTO, responseData);
  };

  public updateAward = async (
    awardId: string,
    awardData: UpdateAwardRequestDTO
  ) => {
    const { name, description, minScore } = awardData;
    const updatedAward = await db.award.update({
      where: { id: awardId },
      data: {
        ...(name && { name }),
        ...(description && { description }),
        ...(minScore && { minScore })
      },
      include: { awardPresenter: { include: { user: true } } }
    });

    const responseData = {
      message: 'Award updated successfully',
      updatedAward: formatAwardResponse(updatedAward)
    } as UpdateAwardResponseDTO;

    return plainToInstance(UpdateAwardResponseDTO, responseData);
  };

  public deleteAward = async (awardId: string) => {
    const deletedAward = await db.award.delete({
      where: { id: awardId },
      include: { awardPresenter: { include: { user: true } } }
    });

    const responseData = {
      message: 'Award deleted successfully',
      deletedAward: formatAwardResponse(deletedAward)
    } as DeleteAwardResponseDTO;

    return plainToInstance(DeleteAwardResponseDTO, responseData);
  };

  public getAwardsInRoom = async (
    roomId: string,
    userId: string,
    type: 'RECEIVED' | 'UNRECEIVED'
  ) => {
    const currentMember = await db.roomMember.findFirst({
      where: { userId, roomId },
      include: { user: true }
    });

    const filter =
      type === 'RECEIVED' ? { some: { userId } } : { none: { userId } };
    const responseKey = type === 'RECEIVED' ? 'receivedAwards' : 'awards';

    const awards = await db.award.findMany({
      where: {
        roomId,
        awardReceiver: {
          ...filter
        }
      },
      include: { awardPresenter: { include: { user: true } } }
    });

    const responseData = {
      memberInformation: {
        ...currentMember,
        profile: { ...currentMember?.user }
      },
      [responseKey]: awards.map((award) => formatAwardResponse(award))
    };
    const responseInstance =
      type === 'RECEIVED'
        ? plainToInstance(GetReceivedAwardsResponseDTO, responseData)
        : plainToInstance(GetUnReceivedAwardResponseDTO, responseData);

    return responseInstance;
  };

  public retrieveAward = async (awardId: string, memberId: string) => {
    const retrievedAward = await db.award.update({
      where: { id: awardId },
      data: { awardReceiver: { connect: { id: memberId } } },
      include: { awardPresenter: { include: { user: true } } }
    });

    const responseData = {
      message: 'Award retrieved successfully',
      retrievedAward: formatAwardResponse(retrievedAward)
    } as RetrieveAwardResponseDTO;

    return plainToInstance(RetrieveAwardResponseDTO, responseData);
  };
}

export default AwardService;
