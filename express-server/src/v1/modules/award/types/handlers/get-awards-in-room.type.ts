import { TRouterHandler } from 'src/v1/common/types/common.type';

type TGetAwardsInRoomParams = { roomId: string };

type TGetAwardsInRoomHandler = TRouterHandler<object, TGetAwardsInRoomParams>;

export default TGetAwardsInRoomHandler;
