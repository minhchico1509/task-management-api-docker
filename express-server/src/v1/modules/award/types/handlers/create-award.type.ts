import { TRouterHandler } from 'src/v1/common/types/common.type';
import CreateAwardRequestDTO from 'src/v1/modules/award/dto/create-award/CreateAwardRequestDTO';

type TCreateAwardParams = { roomId: string };

type TCreateAwardHandler = TRouterHandler<
  CreateAwardRequestDTO,
  TCreateAwardParams
>;

export default TCreateAwardHandler;
