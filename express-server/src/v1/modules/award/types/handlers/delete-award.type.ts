import { TRouterHandler } from 'src/v1/common/types/common.type';

type TDeleteAwardParams = { awardId: string };

type TDeleteAwardHandler = TRouterHandler<object, TDeleteAwardParams>;

export default TDeleteAwardHandler;
