import { TRouterHandler } from 'src/v1/common/types/common.type';
import RetrieveAwardRequestDTO from 'src/v1/modules/award/dto/retrieve-award/RetrieveAwardRequestDTO';

type TRetrieveAwardParams = { awardId: string };

type TRetrieveAwardHandler = TRouterHandler<
  RetrieveAwardRequestDTO,
  TRetrieveAwardParams
>;

export default TRetrieveAwardHandler;
