import { TRouterHandler } from 'src/v1/common/types/common.type';
import UpdateAwardRequestDTO from 'src/v1/modules/award/dto/update-award/UpdateAwardRequestDTO';

type TUpdateAwardParams = { awardId: string };

type TUpdateAwardHandler = TRouterHandler<
  UpdateAwardRequestDTO,
  TUpdateAwardParams
>;

export default TUpdateAwardHandler;
