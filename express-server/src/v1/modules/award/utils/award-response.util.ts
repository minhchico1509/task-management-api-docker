import AwardResponseDTO from 'src/v1/common/dto/AwardResponseDTO';

export const formatAwardResponse = (award: any) => {
  return {
    ...award,
    awardPrsenter: {
      ...award.awardPresenter,
      profile: {
        ...award.awardPresenter.user
      }
    }
  } as AwardResponseDTO;
};
