import BaseRouter from 'src/v1/common/class/BaseRouter';
import AwardController from 'src/v1/modules/award/controllers/award.controller';
import AwardMiddleware from 'src/v1/modules/award/middlewares/award.middleware';

class AwardRouter extends BaseRouter {
  private awardController: AwardController;
  private awardMiddleware: AwardMiddleware;

  constructor() {
    super();
    this.awardController = new AwardController();
    this.awardMiddleware = new AwardMiddleware();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.post(
      '/award/create/:roomId',
      this.awardMiddleware.createAward,
      this.awardController.createAward
    );
    this.router.put(
      '/award/:awardId',
      this.awardMiddleware.updateAward,
      this.awardController.updateAward
    );
    this.router.delete(
      '/award/:awardId',
      this.awardMiddleware.deleteAward,
      this.awardController.deleteAward
    );
    this.router.get(
      '/award/unreceived/:roomId',
      this.awardMiddleware.getAwardsInRoom,
      this.awardController.getUnReceivedAwardsInRoom
    );
    this.router.get(
      '/award/received/:roomId',
      this.awardMiddleware.getAwardsInRoom,
      this.awardController.getReceivedAwardsInRoom
    );
    this.router.post(
      '/award/retrieve/:awardId',
      this.awardMiddleware.retrieveAward,
      this.awardController.retrieveAward
    );
  }
}

export default AwardRouter;
