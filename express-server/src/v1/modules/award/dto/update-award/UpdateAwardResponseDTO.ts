import { Exclude, Expose, Type } from 'class-transformer';

import AwardResponseDTO from 'src/v1/common/dto/AwardResponseDTO';

@Exclude()
class UpdateAwardResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => AwardResponseDTO)
  updatedAward!: AwardResponseDTO;
}

export default UpdateAwardResponseDTO;
