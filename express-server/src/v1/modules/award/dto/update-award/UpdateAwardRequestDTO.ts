import { Exclude, Expose } from 'class-transformer';
import { IsNumber, IsOptional, IsString, MinLength } from 'class-validator';

@Exclude()
class UpdateAwardRequestDTO {
  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  name?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  description?: string;

  @Expose()
  @IsOptional()
  @IsNumber()
  minScore?: number;
}

export default UpdateAwardRequestDTO;
