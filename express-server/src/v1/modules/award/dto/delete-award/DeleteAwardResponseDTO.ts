import { Exclude, Expose, Type } from 'class-transformer';

import AwardResponseDTO from 'src/v1/common/dto/AwardResponseDTO';

@Exclude()
class DeleteAwardResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => AwardResponseDTO)
  deletedAward!: AwardResponseDTO;
}

export default DeleteAwardResponseDTO;
