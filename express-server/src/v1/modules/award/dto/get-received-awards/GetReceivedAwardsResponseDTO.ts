import { Exclude, Expose, Type } from 'class-transformer';

import AwardResponseDTO from 'src/v1/common/dto/AwardResponseDTO';
import RoomMemberResponseDTO from 'src/v1/common/dto/RoomMemberResponseDTO';

@Exclude()
class GetReceivedAwardsResponseDTO {
  @Expose()
  @Type(() => RoomMemberResponseDTO)
  memberInformation!: RoomMemberResponseDTO;

  @Expose()
  @Type(() => AwardResponseDTO)
  receivedAwards!: AwardResponseDTO[];
}

export default GetReceivedAwardsResponseDTO;
