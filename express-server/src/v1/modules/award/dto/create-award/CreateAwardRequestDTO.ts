import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString, MinLength } from 'class-validator';

@Exclude()
class CreateAwardRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  name!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  description!: string;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  minScore!: number;
}

export default CreateAwardRequestDTO;
