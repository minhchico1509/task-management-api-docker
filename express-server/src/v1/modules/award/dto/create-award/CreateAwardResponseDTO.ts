import { Exclude, Expose, Type } from 'class-transformer';

import AwardResponseDTO from 'src/v1/common/dto/AwardResponseDTO';

@Exclude()
class CreateAwardResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => AwardResponseDTO)
  createdAward!: AwardResponseDTO;
}

export default CreateAwardResponseDTO;
