import { Exclude } from 'class-transformer';

@Exclude()
class RetrieveAwardRequestDTO {
  memberId!: string;
}

export default RetrieveAwardRequestDTO;
