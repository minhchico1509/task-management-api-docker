import { Exclude, Expose, Type } from 'class-transformer';

import AwardResponseDTO from 'src/v1/common/dto/AwardResponseDTO';

@Exclude()
class RetrieveAwardResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => AwardResponseDTO)
  retrievedAward!: AwardResponseDTO;
}

export default RetrieveAwardResponseDTO;
