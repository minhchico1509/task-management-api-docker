import { EHeaderKey, EHttpStatusCodes } from 'src/v1/common/constants/enum';
import { handleError } from 'src/v1/exceptions';
import AwardService from 'src/v1/modules/award/services/award.service';
import TCreateAwardHandler from 'src/v1/modules/award/types/handlers/create-award.type';
import TDeleteAwardHandler from 'src/v1/modules/award/types/handlers/delete-award.type';
import TUpdateAwardHandler from 'src/v1/modules/award/types/handlers/edit-award.type';
import TGetAwardsInRoomParams from 'src/v1/modules/award/types/handlers/get-awards-in-room.type';
import TRetrieveAwardHandler from 'src/v1/modules/award/types/handlers/retrieve-award.type';

class AwardController {
  private awardService: AwardService;

  constructor() {
    this.awardService = new AwardService();
  }

  public createAward: TCreateAwardHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const responseData = await this.awardService.createAward(
        roomId,
        userId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateAward: TUpdateAwardHandler = async (req, res, next) => {
    try {
      const { awardId } = req.params;
      const responseData = await this.awardService.updateAward(
        awardId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteAward: TDeleteAwardHandler = async (req, res, next) => {
    try {
      const { awardId } = req.params;
      const responseData = await this.awardService.deleteAward(awardId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getUnReceivedAwardsInRoom: TGetAwardsInRoomParams = async (
    req,
    res,
    next
  ) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const responseData = await this.awardService.getAwardsInRoom(
        roomId,
        userId,
        'UNRECEIVED'
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getReceivedAwardsInRoom: TGetAwardsInRoomParams = async (
    req,
    res,
    next
  ) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const responseData = await this.awardService.getAwardsInRoom(
        roomId,
        userId,
        'RECEIVED'
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public retrieveAward: TRetrieveAwardHandler = async (req, res, next) => {
    try {
      const { awardId } = req.params;
      const memberId = req.body.memberId as string;
      const responseData = await this.awardService.retrieveAward(
        awardId,
        memberId
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default AwardController;
