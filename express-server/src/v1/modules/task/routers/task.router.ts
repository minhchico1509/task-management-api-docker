import BaseRouter from 'src/v1/common/class/BaseRouter';
import TaskController from 'src/v1/modules/task/controllers/task.controller';
import TaskMiddleware from 'src/v1/modules/task/middlewares/task.middleware';

class TaskRouter extends BaseRouter {
  private taskController: TaskController;
  private taskMiddleware: TaskMiddleware;

  constructor() {
    super();
    this.taskController = new TaskController();
    this.taskMiddleware = new TaskMiddleware();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.get(
      '/task/:roomId',
      this.taskMiddleware.getTasksInRoom,
      this.taskController.getTasksInRoom
    );
    this.router.get(
      '/task/detail/:taskId',
      this.taskMiddleware.getDetailTask,
      this.taskController.getTaskDetail
    );
    this.router.get(
      '/task/assigned/:roomId',
      this.taskMiddleware.getAllAssignedTask,
      this.taskController.getAllAssignedTask
    );
    this.router.post(
      '/task/create/:roomId',
      this.taskMiddleware.createTask,
      this.taskController.createTask
    );
    this.router.post(
      '/task/create-recurring/:roomId',
      this.taskMiddleware.createRecurringTask,
      this.taskController.createRecurringTask
    );
    this.router.post(
      '/task/make-recurring/:taskId',
      this.taskMiddleware.makeTaskRecurring,
      this.taskController.makeTaskRecurring
    );
    this.router.put(
      '/task/update/:taskId',
      this.taskMiddleware.updateTask,
      this.taskController.updateTask
    );
    this.router.put(
      '/task/sub-task/status/:subTaskId',
      this.taskMiddleware.updateSubTaskStatus,
      this.taskController.updateSubTaskStatus
    );
    this.router.delete(
      '/task/:taskId',
      this.taskMiddleware.deleteTask,
      this.taskController.deleteTask
    );
  }
}

export default TaskRouter;
