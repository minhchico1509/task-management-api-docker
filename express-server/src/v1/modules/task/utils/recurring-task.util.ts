import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isBetween from 'dayjs/plugin/isBetween';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';

import { ERecurringType } from 'src/v1/modules/task/constants/task.enum';

dayjs.extend(isSameOrBefore);
dayjs.extend(customParseFormat);
dayjs.extend(isBetween);

export const generateDateSegment = (
  startRecurringDate: Date,
  endRecurringDate: Date,
  startDate: Date,
  dueDate: Date,
  recurringType: ERecurringType
) => {
  const dateSegments = [];
  if (recurringType === ERecurringType.DAILY) {
    const startTime = dayjs(startDate).format('HH:mm:ss');
    const dueTime = dayjs(dueDate).format('HH:mm:ss');
    for (
      let i = dayjs(startRecurringDate);
      i.isSameOrBefore(endRecurringDate);
      i = i.add(1, 'day')
    ) {
      const newStartDate = dayjs(
        `${i.format('YYYY-MM-DD')} ${startTime}`,
        'YYYY-MM-DD HH:mm:ss'
      );
      const newDueDate = dayjs(
        `${i.format('YYYY-MM-DD')} ${dueTime}`,
        'YYYY-MM-DD HH:mm:ss'
      );
      if (
        newStartDate.isBetween(
          startRecurringDate,
          endRecurringDate,
          null,
          '[]'
        ) &&
        newDueDate.isBetween(startRecurringDate, endRecurringDate, null, '[]')
      ) {
        dateSegments.push({
          startDate: new Date(newStartDate.format('YYYY-MM-DD HH:mm:ss')),
          dueDate: new Date(newDueDate.format('YYYY-MM-DD HH:mm:ss'))
        });
      }
    }
  } else {
    for (
      let i = dayjs(startDate), j = dayjs(dueDate);
      i.isSameOrBefore(endRecurringDate) && j.isSameOrBefore(endRecurringDate);
      i = i.add(1, 'week'), j = j.add(1, 'week')
    ) {
      if (
        i.isBetween(startRecurringDate, endRecurringDate, null, '[]') &&
        j.isBetween(startRecurringDate, endRecurringDate, null, '[]')
      ) {
        dateSegments.push({
          startDate: new Date(i.format('YYYY-MM-DD HH:mm:ss')),
          dueDate: new Date(j.format('YYYY-MM-DD HH:mm:ss'))
        });
      }
    }
  }
  return dateSegments;
};
