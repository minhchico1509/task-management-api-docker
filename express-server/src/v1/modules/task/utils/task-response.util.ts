import RoomMemberResponseDTO from 'src/v1/common/dto/RoomMemberResponseDTO';
import TaskResponseDTO from 'src/v1/common/dto/TaskResponseDTO';

export const formatTaskResponse = (task: any) => {
  const assignerResponse: RoomMemberResponseDTO = {
    id: task.assigner.id,
    profile: { ...task.assigner.user },
    role: task.assigner.role,
    accumulatedScore: task.assigner.accumulatedScore,
    joinedAt: task.assigner.joinedAt
  };

  const performersResponse: RoomMemberResponseDTO[] = task.performers.map(
    (performer: any) => ({
      id: performer.id,
      profile: { ...performer.user },
      role: performer.role,
      accumulatedScore: performer.accumulatedScore,
      joinedAt: performer.joinedAt
    })
  );

  return {
    ...task,
    assigner: assignerResponse,
    subTasks: task.subTasks,
    performers: performersResponse
  } as TaskResponseDTO;
};
