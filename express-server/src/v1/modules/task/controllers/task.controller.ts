import { EHeaderKey, EHttpStatusCodes } from 'src/v1/common/constants/enum';
import { handleError } from 'src/v1/exceptions';
import TaskService from 'src/v1/modules/task/services/task.service';
import TCreateRecurringTaskHandler from 'src/v1/modules/task/types/handlers/create-recurring-task.type';
import TCreateTaskHandler from 'src/v1/modules/task/types/handlers/create-task.type';
import TDeleteTaskHandler from 'src/v1/modules/task/types/handlers/delete-task.type';
import TGetAllAssignedTaskHandler from 'src/v1/modules/task/types/handlers/get-all-assigned-task.type';
import TGetDetailTaskHandler from 'src/v1/modules/task/types/handlers/get-detail-task.type';
import TGetTasksInRoomHandler from 'src/v1/modules/task/types/handlers/get-tasks-in-room.type';
import TMakeTaskRecurringHandler from 'src/v1/modules/task/types/handlers/make-task-recurring.type';
import TUpdateSubTaskStatusHandler from 'src/v1/modules/task/types/handlers/update-subtask-status.type';
import TUpdateTaskHandler from 'src/v1/modules/task/types/handlers/update-task.type';

class TaskController {
  private taskService: TaskService;

  constructor() {
    this.taskService = new TaskService();
  }

  public createTask: TCreateTaskHandler = async (req, res, next) => {
    try {
      const bodyData = req.body;
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const responseData = await this.taskService.createTask(
        userId,
        roomId,
        bodyData
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public createRecurringTask: TCreateRecurringTaskHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const bodyData = req.body;
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const responseData = await this.taskService.createRecurringTask(
        userId,
        roomId,
        bodyData
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public makeTaskRecurring: TMakeTaskRecurringHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const { taskId } = req.params;
      const responseData = await this.taskService.makeTaskRecurring(
        taskId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateTask: TUpdateTaskHandler = async (req, res, next) => {
    try {
      const { taskId } = req.params;
      const responseData = await this.taskService.updateTask(taskId, req.body);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateSubTaskStatus: TUpdateSubTaskStatusHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const { subTaskId } = req.params;
      const responseData = await this.taskService.updateSubTaskStatus(
        subTaskId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteTask: TDeleteTaskHandler = async (req, res, next) => {
    try {
      const { taskId } = req.params;
      const deletedTask = await this.taskService.deleteTask(taskId);
      return res.status(EHttpStatusCodes.OK).json(deletedTask);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getTaskDetail: TGetDetailTaskHandler = async (req, res, next) => {
    try {
      const { taskId } = req.params;
      const responseData = await this.taskService.getDetailTask(taskId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getAllAssignedTask: TGetAllAssignedTaskHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const responseData = await this.taskService.getAllAssignedTask(
        roomId,
        userId,
        req.query
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getTasksInRoom: TGetTasksInRoomHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const query = req.query;
      const responseData = await this.taskService.getTasksInRoom(roomId, query);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default TaskController;
