import { TRouterHandler } from 'src/v1/common/types/common.type';

type TGetDetailTaskParams = { taskId: string };

type TGetDetailTaskHandler = TRouterHandler<object, TGetDetailTaskParams>;

export default TGetDetailTaskHandler;
