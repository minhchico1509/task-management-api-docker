import { TRouterHandler } from 'src/v1/common/types/common.type';
import UpdateTaskRequestDTO from 'src/v1/modules/task/dto/update-task/UpdateTaskRequestDTO';

type TUpdateTaskParams = { taskId: string };

type TUpdateTaskHandler = TRouterHandler<
  UpdateTaskRequestDTO,
  TUpdateTaskParams
>;

export default TUpdateTaskHandler;
