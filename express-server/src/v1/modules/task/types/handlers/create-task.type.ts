import { TRouterHandler } from 'src/v1/common/types/common.type';
import CreateTaskRequestDTO from 'src/v1/modules/task/dto/create-task/CreateTaskRequestDTO';

type TCreateTaskParams = { roomId: string };

type TCreateTaskHandler = TRouterHandler<
  CreateTaskRequestDTO,
  TCreateTaskParams
>;

export default TCreateTaskHandler;
