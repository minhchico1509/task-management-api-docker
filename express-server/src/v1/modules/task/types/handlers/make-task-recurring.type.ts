import { TRouterHandler } from 'src/v1/common/types/common.type';
import MakeTaskRecurringRequestDTO from 'src/v1/modules/task/dto/make-task-recurring/MakeTaskRecurringRequestDTO';

type TMakeTaskRecurringParams = {
  taskId: string;
};

type TMakeTaskRecurringHandler = TRouterHandler<
  MakeTaskRecurringRequestDTO,
  TMakeTaskRecurringParams
>;

export default TMakeTaskRecurringHandler;
