import { TRouterHandler } from 'src/v1/common/types/common.type';
import GetTasksInRoomQueryDTO from 'src/v1/modules/task/dto/get-tasks-in-room/GetTasksInRoomQueryDTO';

type TGetTasksInRoomParams = { roomId: string };

type TGetTasksInRoomHandler = TRouterHandler<
  object,
  TGetTasksInRoomParams,
  GetTasksInRoomQueryDTO
>;

export default TGetTasksInRoomHandler;
