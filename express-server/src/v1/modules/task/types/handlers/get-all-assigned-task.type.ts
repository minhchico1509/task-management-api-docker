import { TRouterHandler } from 'src/v1/common/types/common.type';
import GetAssignedTaskQueryDTO from 'src/v1/modules/task/dto/get-assigned-tasks/GetAssignedTaskQueryDTO';

type TGetAllAssignedTaskParams = { roomId: string };
type TGetAllAssignedTaskHandler = TRouterHandler<
  object,
  TGetAllAssignedTaskParams,
  GetAssignedTaskQueryDTO
>;
export default TGetAllAssignedTaskHandler;
