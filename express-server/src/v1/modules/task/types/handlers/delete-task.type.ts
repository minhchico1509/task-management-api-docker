import { TRouterHandler } from 'src/v1/common/types/common.type';

type TDeleteTaskParams = { taskId: string };

type TDeleteTaskHandler = TRouterHandler<object, TDeleteTaskParams>;

export default TDeleteTaskHandler;
