import { TRouterHandler } from 'src/v1/common/types/common.type';
import UpdateSubTaskStatusRequestDTO from 'src/v1/modules/task/dto/update-subtask-status/UpdateSubTaskStatusRequestDTO';

type TUpdateSubTaskStatusParams = { subTaskId: string };

type TUpdateSubTaskStatusHandler = TRouterHandler<
  UpdateSubTaskStatusRequestDTO,
  TUpdateSubTaskStatusParams
>;

export default TUpdateSubTaskStatusHandler;
