// eslint-disable-next-line import/namespace
import { TRouterHandler } from 'src/v1/common/types/common.type';
import CreateRecurringTaskRequestDTO from 'src/v1/modules/task/dto/create-recurring-task/CreateRecurringTaskRequestDTO';

type TCreateRecurringTaskParams = {
  roomId: string;
};

type TCreateRecurringTaskHandler = TRouterHandler<
  CreateRecurringTaskRequestDTO,
  TCreateRecurringTaskParams
>;

export default TCreateRecurringTaskHandler;
