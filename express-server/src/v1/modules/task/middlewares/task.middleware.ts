import dayjs from 'dayjs';

import RequestValidator from 'src/v1/common/class/RequestValidator';
import { EHeaderKey } from 'src/v1/common/constants/enum';
import {
  checkRoomExistance,
  checkUserIsAdminOfRoom,
  checkUserIsMemberOfRoom
} from 'src/v1/common/validations/room.validation';
import { checkSubTaskExistance } from 'src/v1/common/validations/sub-task.validation';
import {
  checkPerformersExistanceInRoom,
  checkTaskExistance,
  checkUserIsPerformerOrAdminOfTask,
  checkValidRecurringTask
} from 'src/v1/common/validations/task.validation';
import { checkIsTaskTypeNameExistInRoom } from 'src/v1/common/validations/task-type.validation';
import { BadRequestException, handleError } from 'src/v1/exceptions';
import CreateRecurringTaskRequestDTO from 'src/v1/modules/task/dto/create-recurring-task/CreateRecurringTaskRequestDTO';
import CreateTaskRequestDTO from 'src/v1/modules/task/dto/create-task/CreateTaskRequestDTO';
import GetAssignedTaskQueryDTO from 'src/v1/modules/task/dto/get-assigned-tasks/GetAssignedTaskQueryDTO';
import GetTasksInRoomQueryDTO from 'src/v1/modules/task/dto/get-tasks-in-room/GetTasksInRoomQueryDTO';
import MakeTaskRecurringRequestDTO from 'src/v1/modules/task/dto/make-task-recurring/MakeTaskRecurringRequestDTO';
import UpdateSubTaskStatusRequestDTO from 'src/v1/modules/task/dto/update-subtask-status/UpdateSubTaskStatusRequestDTO';
import UpdateTaskRequestDTO from 'src/v1/modules/task/dto/update-task/UpdateTaskRequestDTO';
import TCreateRecurringTaskHandler from 'src/v1/modules/task/types/handlers/create-recurring-task.type';
import TCreateTaskHandler from 'src/v1/modules/task/types/handlers/create-task.type';
import TDeleteTaskHandler from 'src/v1/modules/task/types/handlers/delete-task.type';
import TGetAllAssignedTaskHandler from 'src/v1/modules/task/types/handlers/get-all-assigned-task.type';
import TGetDetailTaskHandler from 'src/v1/modules/task/types/handlers/get-detail-task.type';
import TGetTasksInRoomHandler from 'src/v1/modules/task/types/handlers/get-tasks-in-room.type';
import TMakeTaskRecurringHandler from 'src/v1/modules/task/types/handlers/make-task-recurring.type';
import TUpdateSubTaskStatusHandler from 'src/v1/modules/task/types/handlers/update-subtask-status.type';
import TUpdateTaskHandler from 'src/v1/modules/task/types/handlers/update-task.type';

class TaskMiddleware {
  constructor() {}

  public createTask: TCreateTaskHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkRoomExistance(roomId);
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(CreateTaskRequestDTO, req);
      const { performers } = req.body;
      await checkPerformersExistanceInRoom(performers, roomId);
      await checkIsTaskTypeNameExistInRoom(roomId, req.body.taskTypeName, true);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public createRecurringTask: TCreateRecurringTaskHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkRoomExistance(roomId);
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(CreateRecurringTaskRequestDTO, req);

      const {
        recurringType,
        startRecurringDate,
        endRecurringDate,
        startDate,
        dueDate
      } = req.body;

      checkValidRecurringTask(
        startRecurringDate,
        endRecurringDate,
        startDate,
        dueDate,
        recurringType
      );

      const { performers } = req.body;
      await checkPerformersExistanceInRoom(performers, roomId);
      await checkIsTaskTypeNameExistInRoom(roomId, req.body.taskTypeName, true);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public makeTaskRecurring: TMakeTaskRecurringHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const { taskId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { roomId } = await checkTaskExistance(taskId);
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(MakeTaskRecurringRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateTask: TUpdateTaskHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { taskId } = req.params;
      const {
        taskTypeName,
        startDate: updateStartDate,
        dueDate: updateDueDate
      } = req.body;
      const {
        roomId,
        startDate: currentStartDate,
        dueDate: currentDueDate
      } = await checkTaskExistance(taskId);
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(UpdateTaskRequestDTO, req);

      if (updateStartDate && updateDueDate) {
        if (dayjs(updateDueDate).isBefore(dayjs(updateStartDate))) {
          throw new BadRequestException('Due date must be after start date');
        }
      } else if (updateStartDate && !updateDueDate) {
        if (dayjs(updateStartDate).isAfter(dayjs(currentDueDate))) {
          throw new BadRequestException('Start date must be before due date');
        }
      } else if (!updateStartDate && updateDueDate) {
        if (dayjs(currentStartDate).isAfter(dayjs(updateDueDate))) {
          throw new BadRequestException('Start date must be before due date');
        }
      }

      if (taskTypeName) {
        const { taskTypeId } = await checkIsTaskTypeNameExistInRoom(
          roomId,
          taskTypeName,
          true
        );
        req.body.taskTypeId = taskTypeId as string;
      }
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateSubTaskStatus: TUpdateSubTaskStatusHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { subTaskId } = req.params;
      const { status } = req.body;
      await RequestValidator.validateJSON(UpdateSubTaskStatusRequestDTO, req);
      const { taskId, subTaskStatus } = await checkSubTaskExistance(subTaskId);
      await checkUserIsPerformerOrAdminOfTask(taskId, userId);
      if (status === subTaskStatus) {
        throw new BadRequestException('Sub task status not change');
      }
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteTask: TDeleteTaskHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { taskId } = req.params;
      await checkTaskExistance(taskId);
      await checkUserIsPerformerOrAdminOfTask(taskId, userId);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getDetailTask: TGetDetailTaskHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { taskId } = req.params;
      await checkTaskExistance(taskId);
      await checkUserIsPerformerOrAdminOfTask(taskId, userId);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getAllAssignedTask: TGetAllAssignedTaskHandler = async (
    req,
    res,
    next
  ) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { roomId } = req.params;
      await checkRoomExistance(roomId);
      await checkUserIsMemberOfRoom(userId, roomId, true);
      await RequestValidator.validateJSON(
        GetAssignedTaskQueryDTO,
        req,
        'QUERY'
      );
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getTasksInRoom: TGetTasksInRoomHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkRoomExistance(roomId);
      await checkUserIsMemberOfRoom(userId, roomId, true);
      await RequestValidator.validateJSON(GetTasksInRoomQueryDTO, req, 'QUERY');
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default TaskMiddleware;
