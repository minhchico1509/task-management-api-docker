import { TaskPriority, TaskStatus } from '@prisma/client';
import { Exclude, Expose, Transform } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsEnum,
  IsOptional,
  IsString,
  MinLength
} from 'class-validator';

@Exclude()
class GetAssignedTaskQueryDTO {
  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  title?: string;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => (typeof value === 'string' ? [value] : value))
  @IsArray()
  @IsEnum(TaskPriority, { each: true })
  priority?: TaskPriority[];

  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  taskType?: string;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => {
    if (value === 'true') return true;
    if (value === 'false') return false;
    return value;
  })
  @IsBoolean()
  isRecurring?: boolean;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => (typeof value === 'string' ? [value] : value))
  @IsArray()
  @IsEnum(TaskStatus, { each: true })
  status?: TaskStatus[];
}

export default GetAssignedTaskQueryDTO;
