import { Exclude, Expose, Type } from 'class-transformer';

import TaskResponseDTO from 'src/v1/common/dto/TaskResponseDTO';
import UserResponseDTO from 'src/v1/common/dto/UserResponseDTO';

@Exclude()
class GetAllAssignedTaskResponseDTO {
  @Expose()
  @Type(() => UserResponseDTO)
  profile!: UserResponseDTO;

  @Expose()
  totalAssignedTasks!: number;

  @Expose()
  @Type(() => TaskResponseDTO)
  assignedTasks!: TaskResponseDTO[];
}

export default GetAllAssignedTaskResponseDTO;
