import { Exclude, Expose, Type } from 'class-transformer';

import TaskResponseDTO from 'src/v1/common/dto/TaskResponseDTO';

@Exclude()
class DeleteTaskResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => TaskResponseDTO)
  deletedTask!: TaskResponseDTO;
}

export default DeleteTaskResponseDTO;
