import { TaskPriority } from '@prisma/client';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsDateString,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  MinLength,
  ValidateNested
} from 'class-validator';

import { CreateSubTaskRequestDTO } from 'src/v1/modules/task/dto/create-task/CreateTaskRequestDTO';

@Exclude()
class UpdateTaskRequestDTO {
  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  title?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  description?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  taskTypeName?: string;

  @Expose()
  @IsOptional()
  @IsEnum(TaskPriority)
  priority?: TaskPriority;

  @Expose()
  @IsOptional()
  @IsDateString()
  startDate?: Date;

  @Expose()
  @IsOptional()
  @IsDateString()
  dueDate?: Date;

  @Expose()
  @IsOptional()
  @IsNumber()
  score?: number;

  @Expose()
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => CreateSubTaskRequestDTO)
  subTasks?: CreateSubTaskRequestDTO[];

  taskTypeId!: string;
}

export default UpdateTaskRequestDTO;
