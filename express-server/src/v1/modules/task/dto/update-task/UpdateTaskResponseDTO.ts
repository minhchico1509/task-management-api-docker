import { Exclude, Expose, Type } from 'class-transformer';

import TaskResponseDTO from 'src/v1/common/dto/TaskResponseDTO';

@Exclude()
class UpdateTaskResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => TaskResponseDTO)
  updatedTask!: TaskResponseDTO;
}

export default UpdateTaskResponseDTO;
