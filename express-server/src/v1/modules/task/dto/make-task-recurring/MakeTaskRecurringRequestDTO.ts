import { Exclude, Expose } from 'class-transformer';
import { IsDateString, IsEnum, IsNotEmpty } from 'class-validator';

import { IsAfterDateTime } from 'src/v1/common/custom-class-validator/date.validator';
import { ERecurringType } from 'src/v1/modules/task/constants/task.enum';

@Exclude()
class MakeTaskRecurringRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsDateString()
  startRecurringDate!: Date;

  @Expose()
  @IsNotEmpty()
  @IsDateString()
  @IsAfterDateTime({ relatedProperty: 'startRecurringDate' })
  endRecurringDate!: Date;

  @Expose()
  @IsNotEmpty()
  @IsEnum(ERecurringType)
  recurringType!: ERecurringType;
}

export default MakeTaskRecurringRequestDTO;
