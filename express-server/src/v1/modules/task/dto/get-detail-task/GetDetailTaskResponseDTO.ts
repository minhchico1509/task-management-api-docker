import { Exclude, Expose, Type } from 'class-transformer';

import RoomResponseDTO from 'src/v1/common/dto/RoomResponseDTO';
import TaskResponseDTO from 'src/v1/common/dto/TaskResponseDTO';

@Exclude()
class GetDetailTaskResponseDTO {
  @Expose()
  @Type(() => RoomResponseDTO)
  room!: RoomResponseDTO;

  @Expose()
  @Type(() => TaskResponseDTO)
  task!: TaskResponseDTO;
}

export default GetDetailTaskResponseDTO;
