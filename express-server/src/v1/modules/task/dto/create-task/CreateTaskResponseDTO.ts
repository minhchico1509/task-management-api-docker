import { Exclude, Expose, Type } from 'class-transformer';

import TaskResponseDTO from 'src/v1/common/dto/TaskResponseDTO';

@Exclude()
class CreateTaskResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => TaskResponseDTO)
  createdTask!: TaskResponseDTO;
}

export default CreateTaskResponseDTO;
