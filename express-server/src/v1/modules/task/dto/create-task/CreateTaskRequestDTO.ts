import { TaskPriority } from '@prisma/client';
import { Exclude, Expose, Transform, Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
  ValidateIf,
  ValidateNested
} from 'class-validator';

import { IsAfterDateTime } from 'src/v1/common/custom-class-validator/date.validator';

@Exclude()
class CreateSubTaskRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  description!: string;
}

@Exclude()
class CreateTaskRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  title!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  description!: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  taskTypeName!: string;

  @Expose()
  @IsNotEmpty()
  @IsEnum(TaskPriority)
  priority!: TaskPriority;

  @Expose()
  @IsNotEmpty()
  @IsDateString()
  startDate!: Date;

  @Expose()
  @IsNotEmpty()
  @IsDateString()
  @IsAfterDateTime({ relatedProperty: 'startDate' })
  dueDate!: Date;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  score!: number;

  @Expose()
  @IsNotEmpty()
  @ValidateIf((o) => !!o.performers)
  @IsArray()
  @ArrayMinSize(1)
  @IsUUID('all', { each: true })
  @Transform(({ value }) => Array.from(new Set(value)))
  performers!: string[];

  @Expose()
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => CreateSubTaskRequestDTO)
  subTasks!: CreateSubTaskRequestDTO[];
}

export default CreateTaskRequestDTO;
export { CreateSubTaskRequestDTO };
