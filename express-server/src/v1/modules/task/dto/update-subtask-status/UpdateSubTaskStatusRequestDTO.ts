import { SubTaskState } from '@prisma/client';
import { Exclude, Expose } from 'class-transformer';
import { IsEnum, IsNotEmpty } from 'class-validator';

@Exclude()
class UpdateSubTaskStatusRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsEnum(SubTaskState)
  status!: SubTaskState;
}

export default UpdateSubTaskStatusRequestDTO;
