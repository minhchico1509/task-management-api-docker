import { Exclude, Expose, Type } from 'class-transformer';

import SubTaskResponseDTO from 'src/v1/common/dto/SubTaskResponseDTO';

@Exclude()
class UpdateSubTaskStatusResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => SubTaskResponseDTO)
  updatedSubTask!: SubTaskResponseDTO;
}

export default UpdateSubTaskStatusResponseDTO;
