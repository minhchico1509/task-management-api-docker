import { TaskPriority, TaskStatus } from '@prisma/client';
import { Expose, Transform } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsEnum,
  IsOptional,
  IsString,
  MinLength
} from 'class-validator';

@Expose()
class GetTasksInRoomQueryDTO {
  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  title?: string;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => (typeof value === 'string' ? [value] : value))
  @IsArray()
  @IsEnum(TaskPriority, { each: true })
  priority?: TaskPriority[];

  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  taskType?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  performerName?: string;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => {
    if (value === 'true') return true;
    if (value === 'false') return false;
    return value;
  })
  @IsBoolean()
  isRecurring?: boolean;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => (typeof value === 'string' ? [value] : value))
  @IsArray()
  @IsEnum(TaskStatus, { each: true })
  status?: TaskStatus[];
}

export default GetTasksInRoomQueryDTO;
