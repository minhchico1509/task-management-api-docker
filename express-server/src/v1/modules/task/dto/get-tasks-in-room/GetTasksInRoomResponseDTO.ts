import { Exclude, Expose, Type } from 'class-transformer';

import TaskResponseDTO from 'src/v1/common/dto/TaskResponseDTO';

@Exclude()
class GetTasksInRoomResponseDTO {
  @Expose()
  totalTasks!: number;

  @Expose()
  @Type(() => TaskResponseDTO)
  tasks!: TaskResponseDTO[];
}

export default GetTasksInRoomResponseDTO;
