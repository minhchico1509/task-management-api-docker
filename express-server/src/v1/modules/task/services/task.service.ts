import { TaskPriority, TaskStatus } from '@prisma/client';
import { plainToInstance } from 'class-transformer';

import { db } from 'src/v1/common/instances/prisma';
import {
  getDeviceTokensFromUserIds,
  sendNotification
} from 'src/v1/common/utils/common.util';
import { checkValidRecurringTask } from 'src/v1/common/validations/task.validation';
import { ERecurringType } from 'src/v1/modules/task/constants/task.enum';
import CreateRecurringTaskRequestDTO from 'src/v1/modules/task/dto/create-recurring-task/CreateRecurringTaskRequestDTO';
import CreateTaskRequestDTO from 'src/v1/modules/task/dto/create-task/CreateTaskRequestDTO';
import CreateTaskResponseDTO from 'src/v1/modules/task/dto/create-task/CreateTaskResponseDTO';
import DeleteTaskResponseDTO from 'src/v1/modules/task/dto/delete-task/DeleteTaskResponseDTO';
import GetAllAssignedTaskResponseDTO from 'src/v1/modules/task/dto/get-assigned-tasks/GetAllAssignedTaskResponseDTO';
import GetAssignedTaskQueryDTO from 'src/v1/modules/task/dto/get-assigned-tasks/GetAssignedTaskQueryDTO';
import GetDetailTaskResponseDTO from 'src/v1/modules/task/dto/get-detail-task/GetDetailTaskResponseDTO';
import GetTasksInRoomQueryDTO from 'src/v1/modules/task/dto/get-tasks-in-room/GetTasksInRoomQueryDTO';
import GetTasksInRoomResponseDTO from 'src/v1/modules/task/dto/get-tasks-in-room/GetTasksInRoomResponseDTO';
import MakeTaskRecurringRequestDTO from 'src/v1/modules/task/dto/make-task-recurring/MakeTaskRecurringRequestDTO';
import UpdateSubTaskStatusRequestDTO from 'src/v1/modules/task/dto/update-subtask-status/UpdateSubTaskStatusRequestDTO';
import UpdateSubTaskStatusResponseDTO from 'src/v1/modules/task/dto/update-subtask-status/UpdateSubTaskStatusResponseDTO';
import UpdateTaskRequestDTO from 'src/v1/modules/task/dto/update-task/UpdateTaskRequestDTO';
import UpdateTaskResponseDTO from 'src/v1/modules/task/dto/update-task/UpdateTaskResponseDTO';
import { generateDateSegment } from 'src/v1/modules/task/utils/recurring-task.util';
import { formatTaskResponse } from 'src/v1/modules/task/utils/task-response.util';

class TaskService {
  constructor() {}

  public createTask = async (
    userId: string,
    roomId: string,
    taskData: CreateTaskRequestDTO
  ) => {
    const {
      title,
      description,
      dueDate,
      performers,
      priority,
      score,
      startDate,
      subTasks,
      taskTypeName
    } = taskData;

    const taskType = await db.taskType.findFirst({
      where: { name: taskTypeName, roomId },
      select: { id: true }
    });

    const performerMembers = await db.roomMember.findMany({
      where: { roomId, userId: { in: performers } },
      select: { id: true, user: { select: { id: true } } }
    });

    const assignerMember = await db.roomMember.findFirst({
      where: { roomId, userId },
      select: { id: true }
    });

    const createdTask = await db.task.create({
      data: {
        title,
        description,
        dueDate,
        priority,
        score,
        startDate,
        room: { connect: { id: roomId } },
        assigner: { connect: { id: assignerMember?.id } },
        taskType: { connect: { id: taskType?.id } },
        subTasks: {
          createMany: { data: subTasks }
        },
        performers: {
          connect: performerMembers.map(({ id }) => ({ id }))
        }
      },
      include: {
        subTasks: true,
        assigner: { include: { user: true } },
        performers: { include: { user: true } },
        taskType: true
      }
    });

    const performerUserIds = performerMembers.map((p) => p.user.id);
    const deviceTokens = await getDeviceTokensFromUserIds(performerUserIds);
    const notificationTitle = `Nhiệm vụ mới từ ${createdTask.assigner.user.fullName}`;
    const notificationBody = `Bạn đã được giao nhiệm vụ mới: ${createdTask.title}`;

    await sendNotification(deviceTokens, notificationTitle, notificationBody);

    const responseData: CreateTaskResponseDTO = {
      message: 'Create task successfully',
      createdTask: formatTaskResponse(createdTask)
    };

    return plainToInstance(CreateTaskResponseDTO, responseData);
  };

  public createRecurringTask = async (
    userId: string,
    roomId: string,
    taskData: CreateRecurringTaskRequestDTO
  ) => {
    const {
      title,
      description,
      dueDate,
      performers,
      priority,
      score,
      startDate,
      subTasks,
      taskTypeName,
      startRecurringDate,
      endRecurringDate,
      recurringType
    } = taskData;

    const taskType = await db.taskType.findFirst({
      where: { name: taskTypeName, roomId },
      select: { id: true }
    });

    const performerMembers = await db.roomMember.findMany({
      where: { roomId, userId: { in: performers } },
      select: { id: true, user: { select: { id: true } } }
    });

    const assignerMember = await db.roomMember.findFirst({
      where: { roomId, userId },
      select: { id: true }
    });

    const dateSegments = generateDateSegment(
      startRecurringDate,
      endRecurringDate,
      startDate,
      dueDate,
      recurringType
    );

    if (dateSegments.length) {
      const data = dateSegments.map(({ startDate, dueDate }) => ({
        title,
        description,
        dueDate,
        startDate,
        isRecurring: true,
        priority,
        score,
        room: { connect: { id: roomId } },
        assigner: { connect: { id: assignerMember?.id } },
        taskType: { connect: { id: taskType?.id } },
        subTasks: {
          createMany: { data: subTasks }
        },
        performers: {
          connect: performerMembers.map(({ id }) => ({ id }))
        }
      }));

      for (const task of data) {
        await db.task.create({
          data: task
        });
      }

      const performerUserIds = performerMembers.map((p) => p.user.id);
      const deviceTokens = await getDeviceTokensFromUserIds(performerUserIds);

      const notificationTitle = `Nhiệm vụ ${recurringType === ERecurringType.DAILY ? 'hàng ngày' : 'hàng tuần'} đã được giao cho bạn`;
      const notificationBody = `Bạn đã được giao nhiệm vụ ${recurringType === ERecurringType.DAILY ? 'hàng ngày' : 'hàng tuần'}`;
      await sendNotification(deviceTokens, notificationTitle, notificationBody);
      return {
        message: 'Create recurring task successfully'
      };
    }
    return {
      message: 'No task created'
    };
  };

  public makeTaskRecurring = async (
    taskId: string,
    bodyData: MakeTaskRecurringRequestDTO
  ) => {
    const { startRecurringDate, endRecurringDate, recurringType } = bodyData;

    const task = await db.task.findUnique({
      where: { id: taskId },
      select: {
        title: true,
        description: true,
        dueDate: true,
        startDate: true,
        priority: true,
        score: true,
        roomId: true,
        assignerId: true,
        taskTypeId: true,
        subTasks: true,
        performers: {
          select: { id: true, user: { select: { id: true } } }
        }
      }
    });

    checkValidRecurringTask(
      startRecurringDate,
      endRecurringDate,
      task?.startDate || new Date(),
      task?.dueDate || new Date(),
      recurringType
    );

    const subTasksData = task?.subTasks.map(({ description }) => ({
      description
    })) as { description: string }[];

    const performersData = task?.performers.map(({ id }) => ({ id })) as {
      id: string;
    }[];

    const dateSegments = generateDateSegment(
      startRecurringDate,
      endRecurringDate,
      task?.startDate || new Date(),
      task?.dueDate || new Date(),
      recurringType
    );

    if (dateSegments.length) {
      const data = dateSegments.map(({ startDate, dueDate }) => ({
        title: task?.title || '',
        description: task?.description || '',
        dueDate,
        startDate,
        isRecurring: true,
        priority: task?.priority || TaskPriority.LOW,
        score: task?.score || 0,
        roomId: task?.roomId || '',
        assignerId: task?.assignerId || '',
        taskTypeId: task?.taskTypeId || '',
        subTasks: {
          createMany: { data: subTasksData }
        },
        performers: {
          connect: performersData.map(({ id }) => ({ id }))
        }
      }));

      for (const task of data) {
        await db.task.create({
          data: task
        });
      }

      const performerUserIds = task?.performers.map((p) => p.user.id) || [];
      const deviceTokens = await getDeviceTokensFromUserIds(performerUserIds);

      const notificationTitle = `Nhiệm vụ ${recurringType === ERecurringType.DAILY ? 'hàng ngày' : 'hàng tuần'} đã được giao cho bạn`;
      const notificationBody = `Bạn đã được giao nhiệm vụ ${recurringType === ERecurringType.DAILY ? 'hàng ngày' : 'hàng tuần'}`;
      await sendNotification(deviceTokens, notificationTitle, notificationBody);
      return {
        message: 'Create recurring task successfully'
      };
    }
  };

  public updateTask = async (
    taskId: string,
    taskData: UpdateTaskRequestDTO
  ) => {
    const {
      title,
      description,
      dueDate,
      priority,
      score,
      startDate,
      subTasks,
      taskTypeName
    } = taskData;

    const { taskTypeId } = taskData;

    if (subTasks) {
      const deletedSubTasks = await db.subTask.deleteMany({
        where: {
          taskId: taskId
        }
      });
    }

    const updatedTask = await db.task.update({
      where: { id: taskId },
      data: {
        ...(title && { title }),
        ...(description && { description }),
        ...(dueDate && { dueDate }),
        ...(priority && { priority }),
        ...(score && { score }),
        ...(startDate && { startDate }),
        ...(taskTypeName && { taskType: { connect: { id: taskTypeId } } }),
        ...(subTasks && {
          subTasks: {
            createMany: { data: subTasks }
          }
        })
      },
      include: {
        subTasks: true,
        assigner: { include: { user: true } },
        performers: { include: { user: true } },
        taskType: true
      }
    });

    const responseData: UpdateTaskResponseDTO = {
      message: 'Update task successfully',
      updatedTask: formatTaskResponse(updatedTask)
    };
    return plainToInstance(UpdateTaskResponseDTO, responseData);
  };

  public updateSubTaskStatus = async (
    subTaskId: string,
    bodyData: UpdateSubTaskStatusRequestDTO
  ) => {
    const { status } = bodyData;
    const updatedSubTask = await db.subTask.update({
      where: { id: subTaskId },
      data: { state: status },
      include: { task: { select: { id: true } } }
    });

    const taskId = updatedSubTask.taskId;

    const task = await db.task.findUnique({
      where: { id: taskId },
      include: { subTasks: true }
    });

    const isAllSubTasksDone = task?.subTasks.every(
      (subTask) => subTask.state === 'DONE'
    );

    if (isAllSubTasksDone) {
      await this.updateTaskStatus(taskId, 'DONE');
    } else {
      await this.updateTaskStatus(taskId, 'IN_PROGRESS');
    }

    const responseData: UpdateSubTaskStatusResponseDTO = {
      message: 'Update sub task status successfully',
      updatedSubTask: updatedSubTask
    };

    return plainToInstance(UpdateSubTaskStatusResponseDTO, responseData);
  };

  public deleteTask = async (taskId: string) => {
    const deletedTask = await db.task.delete({
      where: { id: taskId },
      include: {
        subTasks: true,
        assigner: { include: { user: true } },
        performers: { include: { user: true } },
        taskType: true
      }
    });

    const responseData: DeleteTaskResponseDTO = {
      message: 'Delete task successfully',
      deletedTask: formatTaskResponse(deletedTask)
    };
    return plainToInstance(DeleteTaskResponseDTO, responseData);
  };

  public getDetailTask = async (taskId: string) => {
    const detailTask = await db.task.findUnique({
      where: { id: taskId },
      include: {
        room: true,
        assigner: { include: { user: true } },
        performers: { include: { user: true } },
        taskType: true,
        subTasks: true
      }
    });

    const responseData = {
      room: detailTask?.room,
      task: formatTaskResponse(detailTask)
    } as GetDetailTaskResponseDTO;

    return plainToInstance(GetDetailTaskResponseDTO, responseData);
  };

  public getAllAssignedTask = async (
    roomId: string,
    userId: string,
    query: GetAssignedTaskQueryDTO
  ) => {
    const { title, taskType, priority, isRecurring, status } = query;

    const userProfile = await db.user.findUnique({
      where: { id: userId }
    });
    const tasks = await db.task.findMany({
      where: {
        roomId,
        performers: { some: { userId } },
        ...(status && { status: { in: status } }),
        ...(title && { title: { contains: title } }),
        ...(taskType && { taskType: { name: { contains: taskType } } }),
        ...(priority && { priority: { in: priority } }),
        ...(typeof isRecurring !== 'undefined' && { isRecurring })
      },
      include: {
        subTasks: true,
        assigner: { include: { user: true } },
        performers: { include: { user: true } },
        taskType: true
      }
    });

    const responseData = {
      profile: userProfile,
      totalAssignedTasks: tasks.length,
      assignedTasks: tasks.map((task) => formatTaskResponse(task))
    } as GetAllAssignedTaskResponseDTO;

    return plainToInstance(GetAllAssignedTaskResponseDTO, responseData);
  };

  public getTasksInRoom = async (
    roomId: string,
    query: GetTasksInRoomQueryDTO
  ) => {
    const { title, taskType, priority, performerName, isRecurring, status } =
      query;

    const tasks = await db.task.findMany({
      where: {
        roomId,
        ...(title && { title: { contains: title } }),
        ...(status && { status: { in: status } }),
        ...(taskType && { taskType: { name: { contains: taskType } } }),
        ...(priority && { priority: { in: priority } }),
        ...(typeof isRecurring !== 'undefined' && { isRecurring }),
        ...(performerName && {
          performers: {
            some: { user: { fullName: { contains: performerName } } }
          }
        })
      },
      include: {
        subTasks: true,
        assigner: { include: { user: true } },
        performers: { include: { user: true } },
        taskType: true
      }
    });

    const responseData = {
      totalTasks: tasks.length,
      tasks: tasks.map((task) => formatTaskResponse(task))
    } as GetTasksInRoomResponseDTO;

    return plainToInstance(GetTasksInRoomResponseDTO, responseData);
  };

  public updateTaskStatus = async (taskId: string, status: TaskStatus) => {
    const task = await db.task.findUnique({
      where: { id: taskId },
      select: { status: true }
    });

    if (task?.status === status) {
      return;
    }

    const updatedTask = await db.task.update({
      where: { id: taskId },
      data: { status },

      select: {
        performers: { select: { id: true } },
        score: true
      }
    });

    const roomMembersIds = updatedTask.performers.map(
      (performer) => performer.id
    );

    if (status === 'DONE' || status === 'IN_PROGRESS') {
      const updateMembersScore = await db.roomMember.updateMany({
        where: { id: { in: roomMembersIds } },
        data: {
          accumulatedScore: {
            [status === 'DONE' ? 'increment' : 'decrement']: updatedTask.score
          }
        }
      });
    }

    return updatedTask;
  };
}

export default TaskService;
