export enum ERecurringType {
  DAILY = 'DAILY',
  WEEKLY = 'WEEKLY'
}
