import { EHttpStatusCodes } from 'src/v1/common/constants/enum';
import { handleError } from 'src/v1/exceptions';
import CommentService from 'src/v1/modules/comment/services/comment.service';
import TCreateCommentHandler from 'src/v1/modules/comment/types/handlers/create-comment.type';
import TDeleteCommentHandler from 'src/v1/modules/comment/types/handlers/delete-comment.type';
import TGetTaskCommentsHandler from 'src/v1/modules/comment/types/handlers/get-task-comments.type';
import TUpdateCommentHandler from 'src/v1/modules/comment/types/handlers/update-comment.type';

class CommentController {
  private commentService: CommentService;

  constructor() {
    this.commentService = new CommentService();
  }

  public createComment: TCreateCommentHandler = async (req, res, next) => {
    try {
      const { taskId } = req.params;
      const responseData = await this.commentService.createComment(
        taskId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateComment: TUpdateCommentHandler = async (req, res, next) => {
    try {
      const { commentId } = req.params;
      const responseData = await this.commentService.updateComment(
        commentId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteComment: TDeleteCommentHandler = async (req, res, next) => {
    try {
      const { commentId } = req.params;
      const responseData = await this.commentService.deleteComment(commentId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getTaskComments: TGetTaskCommentsHandler = async (req, res, next) => {
    try {
      const { taskId } = req.params;
      const responseData = await this.commentService.getTaskComments(taskId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default CommentController;
