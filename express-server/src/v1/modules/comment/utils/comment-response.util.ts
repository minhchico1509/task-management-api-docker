import CommentResponseDTO from 'src/v1/common/dto/CommentResponseDTO';

export const formatCommentResponse = (comment: any) => {
  return {
    ...comment,
    commentator: {
      ...comment.commentator,
      profile: {
        ...comment.commentator.user
      }
    }
  } as CommentResponseDTO;
};
