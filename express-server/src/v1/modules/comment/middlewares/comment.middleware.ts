import RequestValidator from 'src/v1/common/class/RequestValidator';
import { EHeaderKey } from 'src/v1/common/constants/enum';
import {
  checkCommentExistance,
  checkIfUserIsOwnerOfComment
} from 'src/v1/common/validations/comment.validation';
import {
  checkTaskExistance,
  checkUserIsPerformerOrAdminOfTask
} from 'src/v1/common/validations/task.validation';
import { handleError } from 'src/v1/exceptions';
import CreateCommentRequestDTO from 'src/v1/modules/comment/dto/create-comment/CreateCommentRequestDTO';
import UpdateCommentRequestDTO from 'src/v1/modules/comment/dto/update-comment/UpdateCommentRequestDTO';
import TCreateCommentHandler from 'src/v1/modules/comment/types/handlers/create-comment.type';
import TDeleteCommentHandler from 'src/v1/modules/comment/types/handlers/delete-comment.type';
import TGetTaskCommentsHandler from 'src/v1/modules/comment/types/handlers/get-task-comments.type';
import TUpdateCommentHandler from 'src/v1/modules/comment/types/handlers/update-comment.type';

class CommentMiddleware {
  constructor() {}

  public createComment: TCreateCommentHandler = async (req, res, next) => {
    try {
      const { taskId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkTaskExistance(taskId);
      const { memberId } = await checkUserIsPerformerOrAdminOfTask(
        taskId,
        userId
      );
      await RequestValidator.validateJSON(CreateCommentRequestDTO, req);
      req.body.memberId = memberId || '';
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public updateComment: TUpdateCommentHandler = async (req, res, next) => {
    try {
      const { commentId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkCommentExistance(commentId);
      await checkIfUserIsOwnerOfComment(userId, commentId);
      await RequestValidator.validateJSON(UpdateCommentRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteComment: TDeleteCommentHandler = async (req, res, next) => {
    try {
      const { commentId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkCommentExistance(commentId);
      await checkIfUserIsOwnerOfComment(userId, commentId);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getTaskComments: TGetTaskCommentsHandler = async (req, res, next) => {
    try {
      const { taskId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkTaskExistance(taskId);
      await checkUserIsPerformerOrAdminOfTask(taskId, userId);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default CommentMiddleware;
