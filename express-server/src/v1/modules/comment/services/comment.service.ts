import { plainToInstance } from 'class-transformer';

import { db } from 'src/v1/common/instances/prisma';
import CreateCommentRequestDTO from 'src/v1/modules/comment/dto/create-comment/CreateCommentRequestDTO';
import CreateCommentResponseDTO from 'src/v1/modules/comment/dto/create-comment/CreateCommentResponseDTO';
import DeleteCommentResponseDTO from 'src/v1/modules/comment/dto/delete-comment/DeleteCommentResponseDTO';
import GetTaskCommentsResponseDTO from 'src/v1/modules/comment/dto/get-task-comments/GetTaskCommentsResponseDTO';
import UpdateCommentRequestDTO from 'src/v1/modules/comment/dto/update-comment/UpdateCommentRequestDTO';
import UpdateCommentResponseDTO from 'src/v1/modules/comment/dto/update-comment/UpdateCommentResponseDTO';
import { formatCommentResponse } from 'src/v1/modules/comment/utils/comment-response.util';

class CommentService {
  constructor() {}

  public createComment = async (
    taskId: string,
    bodyData: CreateCommentRequestDTO
  ) => {
    const { content, memberId } = bodyData;
    const createdComment = await db.comment.create({
      data: {
        taskId,
        commentatorId: memberId,
        content
      },
      include: { commentator: { include: { user: true } } }
    });

    const responseData = {
      message: 'Create comment successfully',
      comment: formatCommentResponse(createdComment)
    } as CreateCommentResponseDTO;

    return plainToInstance(CreateCommentResponseDTO, responseData);
  };

  public updateComment = async (
    commentId: string,
    commentData: UpdateCommentRequestDTO
  ) => {
    const { content } = commentData;
    const updatedComment = await db.comment.update({
      where: { id: commentId },
      data: {
        ...(content && { content })
      },
      include: { commentator: { include: { user: true } } }
    });

    const responseData = {
      message: 'Update comment successfully',
      updatedComment: formatCommentResponse(updatedComment)
    } as UpdateCommentResponseDTO;

    return plainToInstance(UpdateCommentResponseDTO, responseData);
  };

  public deleteComment = async (commentId: string) => {
    const deletedComment = await db.comment.delete({
      where: { id: commentId },
      include: { commentator: { include: { user: true } } }
    });

    const responseData = {
      message: 'Delete comment successfully',
      deletedComment: formatCommentResponse(deletedComment)
    } as DeleteCommentResponseDTO;

    return plainToInstance(DeleteCommentResponseDTO, responseData);
  };

  public getTaskComments = async (taskId: string) => {
    const comments = await db.comment.findMany({
      where: { taskId },
      include: { commentator: { include: { user: true } } }
    });

    const responseData = {
      comments: comments.map((comment) => formatCommentResponse(comment))
    } as GetTaskCommentsResponseDTO;

    return plainToInstance(GetTaskCommentsResponseDTO, responseData);
  };
}

export default CommentService;
