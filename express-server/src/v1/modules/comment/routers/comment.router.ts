import BaseRouter from 'src/v1/common/class/BaseRouter';
import CommentController from 'src/v1/modules/comment/controllers/comment.controller';
import CommentMiddleware from 'src/v1/modules/comment/middlewares/comment.middleware';

class CommentRouter extends BaseRouter {
  private commentController: CommentController;
  private commentMiddleware: CommentMiddleware;

  constructor() {
    super();
    this.commentController = new CommentController();
    this.commentMiddleware = new CommentMiddleware();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.get(
      '/comment/task/:taskId',
      this.commentMiddleware.getTaskComments,
      this.commentController.getTaskComments
    );
    this.router.post(
      '/comment/create/:taskId',
      this.commentMiddleware.createComment,
      this.commentController.createComment
    );
    this.router.put(
      '/comment/:commentId',
      this.commentMiddleware.updateComment,
      this.commentController.updateComment
    );
    this.router.delete(
      '/comment/:commentId',
      this.commentMiddleware.deleteComment,
      this.commentController.deleteComment
    );
  }
}

export default CommentRouter;
