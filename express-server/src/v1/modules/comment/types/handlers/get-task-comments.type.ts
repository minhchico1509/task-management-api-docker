import { TRouterHandler } from 'src/v1/common/types/common.type';

type TGetTaskCommentsParams = { taskId: string };

type TGetTaskCommentsHandler = TRouterHandler<object, TGetTaskCommentsParams>;

export default TGetTaskCommentsHandler;
