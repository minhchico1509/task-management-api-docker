import { TRouterHandler } from 'src/v1/common/types/common.type';
import UpdateCommentRequestDTO from 'src/v1/modules/comment/dto/update-comment/UpdateCommentRequestDTO';

type TUpdateCommentParams = { commentId: string };

type TUpdateCommentHandler = TRouterHandler<
  UpdateCommentRequestDTO,
  TUpdateCommentParams
>;

export default TUpdateCommentHandler;
