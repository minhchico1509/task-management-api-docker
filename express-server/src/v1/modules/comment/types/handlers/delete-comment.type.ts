import { TRouterHandler } from 'src/v1/common/types/common.type';

type TDeleteCommentParams = { commentId: string };

type TDeleteCommentHandler = TRouterHandler<object, TDeleteCommentParams>;

export default TDeleteCommentHandler;
