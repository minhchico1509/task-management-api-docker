import { TRouterHandler } from 'src/v1/common/types/common.type';
import CreateCommentRequestDTO from 'src/v1/modules/comment/dto/create-comment/CreateCommentRequestDTO';

type TCreateCommentParams = { taskId: string };

type TCreateCommentHandler = TRouterHandler<
  CreateCommentRequestDTO,
  TCreateCommentParams
>;

export default TCreateCommentHandler;
