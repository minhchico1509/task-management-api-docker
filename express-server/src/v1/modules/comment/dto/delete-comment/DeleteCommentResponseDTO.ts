import { Exclude, Expose, Type } from 'class-transformer';

import CommentResponseDTO from 'src/v1/common/dto/CommentResponseDTO';

@Exclude()
class DeleteCommentResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => CommentResponseDTO)
  deletedComment!: CommentResponseDTO;
}

export default DeleteCommentResponseDTO;
