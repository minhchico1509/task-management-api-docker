import { Exclude, Expose } from 'class-transformer';
import { IsOptional, IsString, MinLength } from 'class-validator';

@Exclude()
class UpdateCommentRequestDTO {
  @Expose()
  @IsOptional()
  @IsString()
  @MinLength(1)
  content?: string;
}

export default UpdateCommentRequestDTO;
