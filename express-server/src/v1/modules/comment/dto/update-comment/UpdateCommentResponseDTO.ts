import { Exclude, Expose, Type } from 'class-transformer';

import CommentResponseDTO from 'src/v1/common/dto/CommentResponseDTO';

@Exclude()
class UpdateCommentResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => CommentResponseDTO)
  updatedComment!: CommentResponseDTO;
}

export default UpdateCommentResponseDTO;
