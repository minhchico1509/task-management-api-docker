import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString, MinLength } from 'class-validator';

@Exclude()
class CreateCommentRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  content!: string;

  memberId!: string;
}

export default CreateCommentRequestDTO;
