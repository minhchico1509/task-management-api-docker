import { Exclude, Expose, Type } from 'class-transformer';

import CommentResponseDTO from 'src/v1/common/dto/CommentResponseDTO';

@Exclude()
class CreateCommentResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => CommentResponseDTO)
  comment!: CommentResponseDTO;
}

export default CreateCommentResponseDTO;
