import { Exclude, Expose, Type } from 'class-transformer';

import CommentResponseDTO from 'src/v1/common/dto/CommentResponseDTO';

@Exclude()
class GetTaskCommentsResponseDTO {
  @Expose()
  @Type(() => CommentResponseDTO)
  comments!: CommentResponseDTO[];
}

export default GetTaskCommentsResponseDTO;
