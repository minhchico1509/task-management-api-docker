import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
class UpdateDeviceTokenReqDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  deviceToken!: string;
}

export default UpdateDeviceTokenReqDTO;
