import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
class LogoutRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  deviceToken!: string;
}

export default LogoutRequestDTO;
