import { EHeaderKey, EHttpStatusCodes } from 'src/v1/common/constants/enum';
import { handleError } from 'src/v1/exceptions';
import UserService from 'src/v1/modules/user/services/user.service';
import TAddDeviceTokenHandler from 'src/v1/modules/user/types/handlers/add-device-token.type';
import TLogoutHandler from 'src/v1/modules/user/types/handlers/logout.type';

class UserController {
  private userService: UserService;

  constructor() {
    this.userService = new UserService();
  }

  public updateDeviceToken: TAddDeviceTokenHandler = async (req, res, next) => {
    try {
      const deviceToken = req.body.deviceToken;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await this.userService.addDeviceToken(deviceToken, userId);
      return res
        .status(EHttpStatusCodes.OK)
        .json({ message: 'Device token updated successfully' });
    } catch (error) {
      return handleError(next, error);
    }
  };

  public logout: TLogoutHandler = async (req, res, next) => {
    try {
      const deviceToken = req.body.deviceToken;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await this.userService.logout(deviceToken, userId);
      return res
        .status(EHttpStatusCodes.OK)
        .json({ message: 'User logged out successfully' });
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default UserController;
