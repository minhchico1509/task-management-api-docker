import BaseRouter from 'src/v1/common/class/BaseRouter';
import UserController from 'src/v1/modules/user/controllers/user.controller';
import UserMiddleware from 'src/v1/modules/user/middlewares/user.middleware';

class UserRouter extends BaseRouter {
  private userController: UserController;
  private userMiddleware: UserMiddleware;

  constructor() {
    super();
    this.userController = new UserController();
    this.userMiddleware = new UserMiddleware();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.post(
      '/user/update-device-token',
      this.userMiddleware.updateDeviceToken,
      this.userController.updateDeviceToken
    );
    this.router.post(
      '/user/logout',
      this.userMiddleware.logout,
      this.userController.logout
    );
  }
}

export default UserRouter;
