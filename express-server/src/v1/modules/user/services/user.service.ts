import { redisClient } from 'src/v1/common/instances/redis-client';
import IUserService from 'src/v1/modules/user/types/services/user-service.interface';

class UserService implements IUserService {
  constructor() {}

  public addDeviceToken = async (deviceToken: string, userId: string) => {
    await redisClient.sadd(`device_token:${userId}`, deviceToken);
  };

  public logout = async (deviceToken: string, userId: string) => {
    await redisClient.srem(`device_token:${userId}`, deviceToken);
  };
}

export default UserService;
