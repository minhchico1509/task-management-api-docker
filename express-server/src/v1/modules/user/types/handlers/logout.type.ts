import { TRouterHandler } from 'src/v1/common/types/common.type';
import LogoutRequestDTO from 'src/v1/modules/user/dto/logout/LogoutRequestDTO';

type TLogoutHandler = TRouterHandler<LogoutRequestDTO>;

export default TLogoutHandler;
