import { TRouterHandler } from 'src/v1/common/types/common.type';
import UpdateDeviceTokenReqDTO from 'src/v1/modules/user/dto/update-device-token/UpdateDeviceTokenReqDTO';

type TAddDeviceTokenHandler = TRouterHandler<UpdateDeviceTokenReqDTO>;

export default TAddDeviceTokenHandler;
