interface IUserService {
  addDeviceToken: (deviceToken: string, userId: string) => Promise<void>;
}

export default IUserService;
