import RequestValidator from 'src/v1/common/class/RequestValidator';
import { handleError } from 'src/v1/exceptions';
import LogoutRequestDTO from 'src/v1/modules/user/dto/logout/LogoutRequestDTO';
import UpdateDeviceTokenReqDTO from 'src/v1/modules/user/dto/update-device-token/UpdateDeviceTokenReqDTO';
import TAddDeviceTokenHandler from 'src/v1/modules/user/types/handlers/add-device-token.type';
import TLogoutHandler from 'src/v1/modules/user/types/handlers/logout.type';

class UserMiddleware {
  constructor() {}

  public updateDeviceToken: TAddDeviceTokenHandler = async (req, res, next) => {
    try {
      await RequestValidator.validateJSON(UpdateDeviceTokenReqDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public logout: TLogoutHandler = async (req, res, next) => {
    try {
      await RequestValidator.validateJSON(LogoutRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default UserMiddleware;
