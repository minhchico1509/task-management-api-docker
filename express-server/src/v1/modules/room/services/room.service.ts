import { plainToInstance } from 'class-transformer';

import RoomMemberResponseDTO from 'src/v1/common/dto/RoomMemberResponseDTO';
import { db } from 'src/v1/common/instances/prisma';
import shortUniqueId from 'src/v1/common/instances/short-unique-id';
import CreateRoomRequestDTO from 'src/v1/modules/room/dto/create-room/CreateRoomRequestDTO';
import CreateRoomResponseDTO from 'src/v1/modules/room/dto/create-room/CreateRoomResponseDTO';
import GetAllJoinedRoomResponseDTO from 'src/v1/modules/room/dto/get-all-joined-room/GetAllJoinedRoomResponseDTO';
import GetAllRoomMembersResponseDTO from 'src/v1/modules/room/dto/get-all-members/GetAllRoomMembersResponseDTO';
import JoinRoomResponseDTO from 'src/v1/modules/room/dto/join-room/JoinRoomResponseDTO';

class RoomService {
  constructor() {}

  public createRoom = async (
    userId: string,
    roomData: CreateRoomRequestDTO
  ) => {
    const { name, description } = roomData;
    const createdRoom = await db.room.create({
      data: {
        id: shortUniqueId.randomUUID(),
        name,
        description,
        roomMembers: { create: { userId, role: 'ADMIN' } }
      }
    });

    const responseData = {
      message: 'Room created successfully',
      data: { ...createdRoom }
    } as CreateRoomResponseDTO;
    return plainToInstance(CreateRoomResponseDTO, responseData);
  };

  public getAllJoinedRoom = async (userId: string) => {
    const profile = await db.user.findUnique({
      where: { id: userId }
    });
    const joinedRooms = await db.room.findMany({
      where: { roomMembers: { some: { userId } } },
      include: {
        roomMembers: { where: { userId } },
        _count: { select: { roomMembers: true } }
      }
    });

    const responseData = {
      profile,
      joinedRooms: joinedRooms.map((room) => ({
        role: room.roomMembers[0]?.role,
        room: { ...room, totalMembers: room._count?.roomMembers }
      }))
    } as GetAllJoinedRoomResponseDTO;
    return plainToInstance(GetAllJoinedRoomResponseDTO, responseData);
  };

  public joinRoom = async (userId: string, roomId: string) => {
    const updatedRoom = await db.roomMember.create({
      data: {
        user: {
          connect: { id: userId }
        },
        room: {
          connect: { id: roomId }
        },
        role: 'MEMBER'
      },
      include: { room: true }
    });
    const responseData = {
      message: 'Joining room successfully',
      room: { ...updatedRoom.room }
    } as JoinRoomResponseDTO;
    return plainToInstance(JoinRoomResponseDTO, responseData);
  };

  public getAllMembers = async (roomId: string) => {
    const data = await db.room.findUnique({
      where: { id: roomId },
      include: { roomMembers: { include: { user: true } } }
    });

    const members = data?.roomMembers?.map((member) => ({
      id: member.id,
      profile: {
        id: member.user.id,
        fullName: member.user.fullName,
        email: member.user.email
      },
      joinedAt: member.joinedAt,
      role: member.role,
      accumulatedScore: member.accumulatedScore
    })) as RoomMemberResponseDTO[];

    const responseData = {
      room: { ...data },
      members
    } as GetAllRoomMembersResponseDTO;

    return plainToInstance(GetAllRoomMembersResponseDTO, responseData);
  };
}

export default RoomService;
