import { TRouterHandler } from 'src/v1/common/types/common.type';

type TJoinRoomParams = { roomId: string };
type TJoinRoomHandler = TRouterHandler<object, TJoinRoomParams>;

export default TJoinRoomHandler;
