import { TRouterHandler } from 'src/v1/common/types/common.type';
import CreateRoomRequestDTO from 'src/v1/modules/room/dto/create-room/CreateRoomRequestDTO';

type TCreateRoomHandler = TRouterHandler<CreateRoomRequestDTO>;

export default TCreateRoomHandler;
