import { TRouterHandler } from 'src/v1/common/types/common.type';

type TGetAllMembersParams = { roomId: string };
type TGetAllMembersHandler = TRouterHandler<object, TGetAllMembersParams>;

export default TGetAllMembersHandler;
