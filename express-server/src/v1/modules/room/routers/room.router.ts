import BaseRouter from 'src/v1/common/class/BaseRouter';
import RoomController from 'src/v1/modules/room/controllers/room.controller';
import RoomMiddleware from 'src/v1/modules/room/middlewares/room.middleware';

class RoomRouter extends BaseRouter {
  private roomController: RoomController;
  private roomMiddleware: RoomMiddleware;

  constructor() {
    super();
    this.roomController = new RoomController();
    this.roomMiddleware = new RoomMiddleware();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.post(
      '/room/create',
      this.roomMiddleware.createRoom,
      this.roomController.createRoom
    );
    this.router.get(
      '/room/get-all-joined-room',
      this.roomController.getAllJoinedRoom
    );
    this.router.post(
      '/room/join/:roomId',
      this.roomMiddleware.joinRoom,
      this.roomController.joinRoom
    );
    this.router.get(
      '/room/get-all-members/:roomId',
      this.roomMiddleware.getRoomMembers,
      this.roomController.getAllMembers
    );
  }
}

export default RoomRouter;
