import { EHeaderKey, EHttpStatusCodes } from 'src/v1/common/constants/enum';
import { TRouterHandler } from 'src/v1/common/types/common.type';
import { handleError } from 'src/v1/exceptions';
import RoomService from 'src/v1/modules/room/services/room.service';
import TCreateRoomHandler from 'src/v1/modules/room/types/handlers/create-room.type';
import TGetAllMembersHandler from 'src/v1/modules/room/types/handlers/get-all-members.type';
import TJoinRoomHandler from 'src/v1/modules/room/types/handlers/join-room.type';

class RoomController {
  private roomService: RoomService;

  constructor() {
    this.roomService = new RoomService();
  }

  public createRoom: TCreateRoomHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const roomData = req.body;
      const responseData = await this.roomService.createRoom(userId, roomData);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getAllJoinedRoom: TRouterHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const responseData = await this.roomService.getAllJoinedRoom(userId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public joinRoom: TJoinRoomHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { roomId } = req.params;
      const responseData = await this.roomService.joinRoom(userId, roomId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getAllMembers: TGetAllMembersHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const responseData = await this.roomService.getAllMembers(roomId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default RoomController;
