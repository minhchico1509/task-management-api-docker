import RequestValidator from 'src/v1/common/class/RequestValidator';
import { EHeaderKey } from 'src/v1/common/constants/enum';
import {
  checkRoomExistance,
  checkUserIsMemberOfRoom
} from 'src/v1/common/validations/room.validation';
import { handleError } from 'src/v1/exceptions';
import CreateRoomRequestDTO from 'src/v1/modules/room/dto/create-room/CreateRoomRequestDTO';
import TCreateRoomHandler from 'src/v1/modules/room/types/handlers/create-room.type';
import TGetAllMembersHandler from 'src/v1/modules/room/types/handlers/get-all-members.type';
import TJoinRoomHandler from 'src/v1/modules/room/types/handlers/join-room.type';

class RoomMiddleware {
  constructor() {}

  public createRoom: TCreateRoomHandler = async (req, res, next) => {
    try {
      await RequestValidator.validateJSON(CreateRoomRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public joinRoom: TJoinRoomHandler = async (req, res, next) => {
    try {
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const roomId = req.params.roomId;
      await checkRoomExistance(roomId);
      await checkUserIsMemberOfRoom(userId, roomId, false);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public getRoomMembers: TGetAllMembersHandler = async (req, res, next) => {
    try {
      const roomId = req.params.roomId;
      const userId = req.headers[EHeaderKey.USER_ID] as string;

      await checkRoomExistance(roomId);
      await checkUserIsMemberOfRoom(userId, roomId, true);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default RoomMiddleware;
