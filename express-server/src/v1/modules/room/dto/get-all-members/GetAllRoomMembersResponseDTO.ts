import { Exclude, Expose, Type } from 'class-transformer';

import RoomMemberResponseDTO from 'src/v1/common/dto/RoomMemberResponseDTO';
import RoomResponseDTO from 'src/v1/common/dto/RoomResponseDTO';

@Exclude()
class GetAllRoomMembersResponseDTO {
  @Expose()
  @Type(() => RoomResponseDTO)
  room!: RoomResponseDTO;

  @Expose()
  @Type(() => RoomMemberResponseDTO)
  members!: RoomMemberResponseDTO[];
}

export default GetAllRoomMembersResponseDTO;
