import { Exclude, Expose, Type } from 'class-transformer';

import RoomResponseDTO from 'src/v1/common/dto/RoomResponseDTO';

@Exclude()
class JoinRoomResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => RoomResponseDTO)
  room!: RoomResponseDTO;
}

export default JoinRoomResponseDTO;
