import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString, MinLength } from 'class-validator';

@Exclude()
class CreateRoomRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  name!: string;

  @Expose()
  @IsOptional()
  @MinLength(1)
  @IsString()
  description!: string;
}

export default CreateRoomRequestDTO;
