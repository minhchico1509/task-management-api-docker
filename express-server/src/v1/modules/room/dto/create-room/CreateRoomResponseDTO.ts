import { Exclude, Expose, Type } from 'class-transformer';

import RoomResponseDTO from 'src/v1/common/dto/RoomResponseDTO';

@Exclude()
class CreateRoomResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => RoomResponseDTO)
  data!: RoomResponseDTO;
}

export default CreateRoomResponseDTO;
