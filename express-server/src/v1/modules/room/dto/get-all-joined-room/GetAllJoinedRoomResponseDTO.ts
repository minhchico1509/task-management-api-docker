import { RoomRole } from '@prisma/client';
import { Exclude, Expose, Type } from 'class-transformer';

import RoomResponseDTO from 'src/v1/common/dto/RoomResponseDTO';
import UserResponseDTO from 'src/v1/common/dto/UserResponseDTO';

@Exclude()
class JoinedRoom {
  @Expose()
  role!: RoomRole;

  @Expose()
  @Type(() => RoomResponseDTO)
  room!: RoomResponseDTO;
}

@Exclude()
class GetAllJoinedRoomResponseDTO {
  @Expose()
  @Type(() => UserResponseDTO)
  profile!: UserResponseDTO;

  @Expose()
  @Type(() => JoinedRoom)
  joinedRooms!: JoinedRoom[];
}

export default GetAllJoinedRoomResponseDTO;
