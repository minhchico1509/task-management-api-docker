import BaseRouter from 'src/v1/common/class/BaseRouter';
import TaskTypeController from 'src/v1/modules/task-type/controllers/task-type.controller';
import TaskTypeMiddleware from 'src/v1/modules/task-type/middlewares/task-type.middleware';

class TaskTypeRouter extends BaseRouter {
  private taskTypeController: TaskTypeController;
  private taskTypeMiddleware: TaskTypeMiddleware;

  constructor() {
    super();
    this.taskTypeController = new TaskTypeController();
    this.taskTypeMiddleware = new TaskTypeMiddleware();
    this.initializeRouter();
  }

  private initializeRouter() {
    this.router.get(
      '/task-type/:roomId',
      this.taskTypeMiddleware.getAllTaskTypes,
      this.taskTypeController.getAllTaskType
    );
    this.router.post(
      '/task-type/create/:roomId',
      this.taskTypeMiddleware.createTaskType,
      this.taskTypeController.createTaskType
    );
    this.router.put(
      '/task-type/:taskTypeId',
      this.taskTypeMiddleware.editTaskType,
      this.taskTypeController.editTaskType
    );
    this.router.delete(
      '/task-type/:taskTypeId',
      this.taskTypeMiddleware.deleteTaskType,
      this.taskTypeController.deleteTaskType
    );
  }
}

export default TaskTypeRouter;
