import { plainToInstance } from 'class-transformer';

import { db } from 'src/v1/common/instances/prisma';
import GetAllTaskTypeResponseDTO from 'src/v1/modules/task-type/dto/get-all-task-type/GetAllTaskTypeResponseDTO';
import ModifyTaskTypeRequestDTO from 'src/v1/modules/task-type/dto/modify-task-type/ModifyTaskTypeRequestDTO';
import ModifyTaskTypeResponseDTO from 'src/v1/modules/task-type/dto/modify-task-type/ModifyTaskTypeResponseDTO';

class TaskTypeService {
  constructor() {}

  public getAllTaskTypes = async (roomId: string) => {
    const taskTypes = await db.taskType.findMany({
      where: { roomId },
      include: { room: { select: { id: true, name: true, description: true } } }
    });

    const responseData = {
      room: taskTypes[0]?.room,
      taskTypes
    } as GetAllTaskTypeResponseDTO;

    return plainToInstance(GetAllTaskTypeResponseDTO, responseData);
  };

  public createTaskType = async (
    roomId: string,
    taskTypeData: ModifyTaskTypeRequestDTO
  ) => {
    const { name } = taskTypeData;
    const taskType = await db.taskType.create({
      data: {
        name,
        room: { connect: { id: roomId } }
      }
    });

    const responseData = {
      message: 'Task type created successfully',
      taskType
    } as ModifyTaskTypeResponseDTO;

    return plainToInstance(ModifyTaskTypeResponseDTO, responseData);
  };

  public editTaskType = async (
    taskTypeId: string,
    taskTypeData: ModifyTaskTypeRequestDTO
  ) => {
    const { name } = taskTypeData;
    const taskType = await db.taskType.update({
      where: { id: taskTypeId },
      data: { name }
    });

    const responseData = {
      message: 'Task type updated successfully',
      taskType
    } as ModifyTaskTypeResponseDTO;

    return plainToInstance(ModifyTaskTypeResponseDTO, responseData);
  };

  public deleteTaskType = async (taskTypeId: string) => {
    const deletedTaskType = await db.taskType.delete({
      where: { id: taskTypeId }
    });

    const responseData = {
      message: 'Task type deleted successfully',
      taskType: deletedTaskType
    } as ModifyTaskTypeResponseDTO;

    return plainToInstance(ModifyTaskTypeResponseDTO, responseData);
  };
}

export default TaskTypeService;
