import { EHttpStatusCodes } from 'src/v1/common/constants/enum';
import { handleError } from 'src/v1/exceptions';
import TaskTypeService from 'src/v1/modules/task-type/services/task-type.service';
import TCreateTaskTypeHandler from 'src/v1/modules/task-type/types/handlers/create-task-type.type';
import TDeleteTaskTypeHandler from 'src/v1/modules/task-type/types/handlers/delete-task-type.type';
import TEditTaskTypeHandler from 'src/v1/modules/task-type/types/handlers/edit-task-type.type';
import TGetAllTaskTypesHandler from 'src/v1/modules/task-type/types/handlers/get-all-task-types.type';

class TaskTypeController {
  private taskTypeService: TaskTypeService;

  constructor() {
    this.taskTypeService = new TaskTypeService();
  }

  public getAllTaskType: TGetAllTaskTypesHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const responseData = await this.taskTypeService.getAllTaskTypes(roomId);
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public createTaskType: TCreateTaskTypeHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const responseData = await this.taskTypeService.createTaskType(
        roomId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public editTaskType: TEditTaskTypeHandler = async (req, res, next) => {
    try {
      const { taskTypeId } = req.params;
      const responseData = await this.taskTypeService.editTaskType(
        taskTypeId,
        req.body
      );
      return res.status(EHttpStatusCodes.OK).json(responseData);
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteTaskType: TDeleteTaskTypeHandler = async (req, res, next) => {
    try {
      const { taskTypeId } = req.params;
      await this.taskTypeService.deleteTaskType(taskTypeId);
      return res
        .status(EHttpStatusCodes.OK)
        .json({ message: 'Task type deleted successfully' });
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default TaskTypeController;
