import RequestValidator from 'src/v1/common/class/RequestValidator';
import { EHeaderKey } from 'src/v1/common/constants/enum';
import {
  checkUserIsAdminOfRoom,
  checkUserIsMemberOfRoom
} from 'src/v1/common/validations/room.validation';
import {
  checkIsTaskTypeNameExistInRoom,
  checkTaskTypeExistanceById
} from 'src/v1/common/validations/task-type.validation';
import { handleError } from 'src/v1/exceptions';
import ModifyTaskTypeRequestDTO from 'src/v1/modules/task-type/dto/modify-task-type/ModifyTaskTypeRequestDTO';
import TCreateTaskTypeHandler from 'src/v1/modules/task-type/types/handlers/create-task-type.type';
import TDeleteTaskTypeHandler from 'src/v1/modules/task-type/types/handlers/delete-task-type.type';
import TEditTaskTypeHandler from 'src/v1/modules/task-type/types/handlers/edit-task-type.type';
import TGetAllTaskTypesHandler from 'src/v1/modules/task-type/types/handlers/get-all-task-types.type';

class TaskTypeMiddleware {
  constructor() {}

  public getAllTaskTypes: TGetAllTaskTypesHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkUserIsMemberOfRoom(userId, roomId, true);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public createTaskType: TCreateTaskTypeHandler = async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(ModifyTaskTypeRequestDTO, req);
      await checkIsTaskTypeNameExistInRoom(roomId, req.body.name, false);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public editTaskType: TEditTaskTypeHandler = async (req, res, next) => {
    try {
      const { taskTypeId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { roomId } = await checkTaskTypeExistanceById(taskTypeId);
      await checkUserIsAdminOfRoom(userId, roomId);
      await RequestValidator.validateJSON(ModifyTaskTypeRequestDTO, req);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };

  public deleteTaskType: TDeleteTaskTypeHandler = async (req, res, next) => {
    try {
      const { taskTypeId } = req.params;
      const userId = req.headers[EHeaderKey.USER_ID] as string;
      const { roomId } = await checkTaskTypeExistanceById(taskTypeId);
      await checkUserIsAdminOfRoom(userId, roomId);
      next();
    } catch (error) {
      return handleError(next, error);
    }
  };
}

export default TaskTypeMiddleware;
