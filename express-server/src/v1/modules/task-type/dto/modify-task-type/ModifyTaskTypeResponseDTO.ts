import { Exclude, Expose, Type } from 'class-transformer';

import TaskTypeResponseDTO from 'src/v1/common/dto/TaskTypeResponseDTO';

@Exclude()
class ModifyTaskTypeResponseDTO {
  @Expose()
  message!: string;

  @Expose()
  @Type(() => TaskTypeResponseDTO)
  taskType!: TaskTypeResponseDTO;
}

export default ModifyTaskTypeResponseDTO;
