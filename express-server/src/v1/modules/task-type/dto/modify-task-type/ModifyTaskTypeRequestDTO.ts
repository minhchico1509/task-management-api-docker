import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
class ModifyTaskTypeRequestDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  name!: string;
}

export default ModifyTaskTypeRequestDTO;
