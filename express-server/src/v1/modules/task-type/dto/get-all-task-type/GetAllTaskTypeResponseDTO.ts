import { Exclude, Expose, Type } from 'class-transformer';

import RoomResponseDTO from 'src/v1/common/dto/RoomResponseDTO';
import TaskTypeResponseDTO from 'src/v1/common/dto/TaskTypeResponseDTO';

@Exclude()
class GetAllTaskTypeResponseDTO {
  @Expose()
  @Type(() => RoomResponseDTO)
  room!: RoomResponseDTO;

  @Expose()
  @Type(() => TaskTypeResponseDTO)
  taskTypes!: TaskTypeResponseDTO[];
}

export default GetAllTaskTypeResponseDTO;
