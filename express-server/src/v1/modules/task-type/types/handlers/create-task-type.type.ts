import { TRouterHandler } from 'src/v1/common/types/common.type';
import ModifyTaskTypeRequestDTO from 'src/v1/modules/task-type/dto/modify-task-type/ModifyTaskTypeRequestDTO';

type TCreateTaskTypeParams = { roomId: string };
type TCreateTaskTypeHandler = TRouterHandler<
  ModifyTaskTypeRequestDTO,
  TCreateTaskTypeParams
>;

export default TCreateTaskTypeHandler;
