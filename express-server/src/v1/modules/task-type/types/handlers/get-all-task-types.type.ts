import { TRouterHandler } from 'src/v1/common/types/common.type';

type TGetAllTaskTypesParams = { roomId: string };
type TGetAllTaskTypesHandler = TRouterHandler<object, TGetAllTaskTypesParams>;

export default TGetAllTaskTypesHandler;
