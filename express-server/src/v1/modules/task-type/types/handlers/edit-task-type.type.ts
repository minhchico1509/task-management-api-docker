import { TRouterHandler } from 'src/v1/common/types/common.type';
import ModifyTaskTypeRequestDTO from 'src/v1/modules/task-type/dto/modify-task-type/ModifyTaskTypeRequestDTO';

type TEditTaskTypeParams = { taskTypeId: string };
type TEditTaskTypeHandler = TRouterHandler<
  ModifyTaskTypeRequestDTO,
  TEditTaskTypeParams
>;

export default TEditTaskTypeHandler;
