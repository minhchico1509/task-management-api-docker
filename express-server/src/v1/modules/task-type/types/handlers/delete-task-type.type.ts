import { TRouterHandler } from 'src/v1/common/types/common.type';

type TDeleteTaskTypeParams = { taskTypeId: string };
type TDeleteTaskTypeHandler = TRouterHandler<object, TDeleteTaskTypeParams>;

export default TDeleteTaskTypeHandler;
