import 'reflect-metadata';
import ExpressServer from 'src/v1/server';

const expressServer = new ExpressServer();

expressServer.start();
