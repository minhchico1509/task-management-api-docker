#!/bin/bash

# Đường dẫn đến thư mục modules
MODULE_PATH="./src/v1/modules"

# Kiểm tra xem thư mục có tồn tại hay không
is_directory_exists() {
  local path="$1"
  if [ -d "$path" ]; then
    return 0
  else
    return 1
  fi
}

# Ghi nội dung vào file
write_file() {
  local file_path="$1"
  local content="$2"
  echo "$content" >"$file_path"
}

run() {
  # Yêu cầu người dùng nhập tên module
  read -p "Enter your module: " module_name

  # Viết hoa chữ cái đầu tiên của tên module
  module_name_capitalized=$(echo "${module_name:0:1}" | tr '[:lower:]' '[:upper:]')${module_name:1}

  # Tạo đường dẫn đến thư mục module
  module_folder="${MODULE_PATH}/${module_name}"

  # Kiểm tra xem thư mục module đã tồn tại hay chưa
  if is_directory_exists "$module_folder"; then
    echo "Generate failed: Module already exists"
    return
  fi

  # Tạo nội dung file controller
  controller_content="import { NextFunction, Response } from 'express';

class ${module_name_capitalized}Controller {
  private ${module_name}Service: ${module_name_capitalized}Service;

  constructor() {
    this.${module_name}Service = new ${module_name_capitalized}Service();
  }
}

export default ${module_name_capitalized}Controller;
"

  # Tạo nội dung file middleware
  middleware_content="class ${module_name_capitalized}Middleware {
  constructor() {}
}

export default ${module_name_capitalized}Middleware;
"

  # Tạo nội dung file router
  router_content="class ${module_name_capitalized}Router extends BaseRouter {
  private ${module_name}Controller: ${module_name_capitalized}Controller;
  private ${module_name}Middleware: ${module_name_capitalized}Middleware;

  constructor() {
    super();
    this.${module_name}Controller = new ${module_name_capitalized}Controller();
    this.${module_name}Middleware = new ${module_name_capitalized}Middleware();
    this.initializeRouter();
  }

  private initializeRouter() {}
}

export default ${module_name_capitalized}Router;
"
  # Tạo nội dung file service
  service_content="class ${module_name_capitalized}Service {
  constructor() {}
}

export default ${module_name_capitalized}Service;
"

  # Tạo nội dung file interface service
  service_interface_content="interface I${module_name_capitalized}Service {}

export default I${module_name_capitalized}Service;
"

  # Tạo các thư mục
  folder_list=(
    "controllers"
    "types"
    "middlewares"
    "routers"
    "services"
    "dto"
  )
  for folder in "${folder_list[@]}"; do
    mkdir -p "${module_folder}/${folder}"
  done

  mkdir -p "${module_folder}/types/handlers"
  mkdir -p "${module_folder}/types/services"

  # Ghi nội dung vào các file
  write_file "${module_folder}/controllers/${module_name}.controller.ts" "$controller_content"
  write_file "${module_folder}/middlewares/${module_name}.middleware.ts" "$middleware_content"
  write_file "${module_folder}/routers/${module_name}.router.ts" "$router_content"
  write_file "${module_folder}/services/${module_name}.service.ts" "$service_content"
  write_file "${module_folder}/types/services/${module_name}-service.interface.ts" "$service_interface_content"

  echo "All files have been created successfully!"
}

run
