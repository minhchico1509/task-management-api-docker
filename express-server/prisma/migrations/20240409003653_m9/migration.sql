/*
  Warnings:

  - You are about to alter the column `status` on the `task` table. The data in that column could be lost. The data in that column will be cast from `Enum(EnumId(3))` to `Enum(EnumId(2))`.

*/
-- AlterTable
ALTER TABLE `task` MODIFY `status` ENUM('IN_PROGRESS', 'CANCELLED', 'DONE') NOT NULL DEFAULT 'IN_PROGRESS';
