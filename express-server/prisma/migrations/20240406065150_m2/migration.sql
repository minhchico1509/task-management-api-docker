/*
  Warnings:

  - You are about to drop the `roommember` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `subtask` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `_receive_awards` DROP FOREIGN KEY `_receive_awards_B_fkey`;

-- DropForeignKey
ALTER TABLE `_user_perform_task` DROP FOREIGN KEY `_user_perform_task_A_fkey`;

-- DropForeignKey
ALTER TABLE `award` DROP FOREIGN KEY `Award_award_prsenter_id_fkey`;

-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `Comment_task_id_fkey`;

-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `Comment_user_id_fkey`;

-- DropForeignKey
ALTER TABLE `roommember` DROP FOREIGN KEY `RoomMember_room_id_fkey`;

-- DropForeignKey
ALTER TABLE `roommember` DROP FOREIGN KEY `RoomMember_user_id_fkey`;

-- DropForeignKey
ALTER TABLE `subtask` DROP FOREIGN KEY `SubTask_task_id_fkey`;

-- DropForeignKey
ALTER TABLE `task` DROP FOREIGN KEY `Task_assigner_id_fkey`;

-- DropForeignKey
ALTER TABLE `task` DROP FOREIGN KEY `Task_room_id_fkey`;

-- DropTable
DROP TABLE `roommember`;

-- DropTable
DROP TABLE `subtask`;

-- CreateTable
CREATE TABLE `room_member` (
    `id` VARCHAR(191) NOT NULL,
    `role` ENUM('ADMIN', 'MEMBER') NOT NULL,
    `joined_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `user_id` VARCHAR(191) NOT NULL,
    `room_id` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sub_task` (
    `id` VARCHAR(191) NOT NULL,
    `description` VARCHAR(191) NOT NULL,
    `state` ENUM('IN_PROGRESS', 'DONE') NOT NULL,
    `task_id` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `room_member` ADD CONSTRAINT `room_member_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `room_member` ADD CONSTRAINT `room_member_room_id_fkey` FOREIGN KEY (`room_id`) REFERENCES `room`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `task` ADD CONSTRAINT `task_room_id_fkey` FOREIGN KEY (`room_id`) REFERENCES `room`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `task` ADD CONSTRAINT `task_assigner_id_fkey` FOREIGN KEY (`assigner_id`) REFERENCES `room_member`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `sub_task` ADD CONSTRAINT `sub_task_task_id_fkey` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_task_id_fkey` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `room_member`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `award` ADD CONSTRAINT `award_award_prsenter_id_fkey` FOREIGN KEY (`award_prsenter_id`) REFERENCES `room_member`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_user_perform_task` ADD CONSTRAINT `_user_perform_task_A_fkey` FOREIGN KEY (`A`) REFERENCES `room_member`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_receive_awards` ADD CONSTRAINT `_receive_awards_B_fkey` FOREIGN KEY (`B`) REFERENCES `room_member`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- RenameIndex
ALTER TABLE `user` RENAME INDEX `User_email_key` TO `user_email_key`;
