/*
  Warnings:

  - You are about to drop the column `award_prsenter_id` on the `award` table. All the data in the column will be lost.
  - Added the required column `award_presenter_id` to the `award` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `award` DROP FOREIGN KEY `award_award_prsenter_id_fkey`;

-- AlterTable
ALTER TABLE `award` DROP COLUMN `award_prsenter_id`,
    ADD COLUMN `award_presenter_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `award` ADD CONSTRAINT `award_award_presenter_id_fkey` FOREIGN KEY (`award_presenter_id`) REFERENCES `room_member`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
