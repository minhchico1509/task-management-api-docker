/*
  Warnings:

  - You are about to drop the column `user_id` on the `comment` table. All the data in the column will be lost.
  - Added the required column `member_id` to the `comment` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `comment_user_id_fkey`;

-- AlterTable
ALTER TABLE `comment` DROP COLUMN `user_id`,
    ADD COLUMN `member_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_member_id_fkey` FOREIGN KEY (`member_id`) REFERENCES `room_member`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
