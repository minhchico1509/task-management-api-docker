/*
  Warnings:

  - You are about to drop the column `task_id` on the `comment` table. All the data in the column will be lost.
  - You are about to drop the column `task_id` on the `sub_task` table. All the data in the column will be lost.
  - You are about to drop the column `due_date` on the `task` table. All the data in the column will be lost.
  - You are about to drop the column `start_date` on the `task` table. All the data in the column will be lost.
  - You are about to drop the column `status` on the `task` table. All the data in the column will be lost.
  - Added the required column `task_slot_id` to the `comment` table without a default value. This is not possible if the table is not empty.
  - Added the required column `task_slot_id` to the `sub_task` table without a default value. This is not possible if the table is not empty.
  - Added the required column `task_type_id` to the `task` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `comment_task_id_fkey`;

-- DropForeignKey
ALTER TABLE `sub_task` DROP FOREIGN KEY `sub_task_task_id_fkey`;

-- AlterTable
ALTER TABLE `comment` DROP COLUMN `task_id`,
    ADD COLUMN `task_slot_id` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `sub_task` DROP COLUMN `task_id`,
    ADD COLUMN `task_slot_id` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `task` DROP COLUMN `due_date`,
    DROP COLUMN `start_date`,
    DROP COLUMN `status`,
    ADD COLUMN `task_type_id` VARCHAR(191) NOT NULL;

-- CreateTable
CREATE TABLE `task_type` (
    `id` VARCHAR(191) NOT NULL,
    `name` VARCHAR(191) NOT NULL,
    `room_id` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `task_slot` (
    `id` VARCHAR(191) NOT NULL,
    `status` ENUM('OPEN', 'IN_PROGRESS', 'CANCELLED', 'DONE') NOT NULL,
    `start_date` DATETIME(3) NOT NULL,
    `due_date` DATETIME(3) NOT NULL,
    `task_id` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `task_type` ADD CONSTRAINT `task_type_room_id_fkey` FOREIGN KEY (`room_id`) REFERENCES `room`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `task` ADD CONSTRAINT `task_task_type_id_fkey` FOREIGN KEY (`task_type_id`) REFERENCES `task_type`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `task_slot` ADD CONSTRAINT `task_slot_task_id_fkey` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `sub_task` ADD CONSTRAINT `sub_task_task_slot_id_fkey` FOREIGN KEY (`task_slot_id`) REFERENCES `task_slot`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_task_slot_id_fkey` FOREIGN KEY (`task_slot_id`) REFERENCES `task_slot`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
