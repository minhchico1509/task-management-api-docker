/*
  Warnings:

  - You are about to drop the column `task_slot_id` on the `comment` table. All the data in the column will be lost.
  - You are about to drop the column `task_slot_id` on the `sub_task` table. All the data in the column will be lost.
  - Added the required column `task_id` to the `comment` table without a default value. This is not possible if the table is not empty.
  - Added the required column `task_id` to the `sub_task` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `comment_task_slot_id_fkey`;

-- DropForeignKey
ALTER TABLE `sub_task` DROP FOREIGN KEY `sub_task_task_slot_id_fkey`;

-- AlterTable
ALTER TABLE `comment` DROP COLUMN `task_slot_id`,
    ADD COLUMN `task_id` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `sub_task` DROP COLUMN `task_slot_id`,
    ADD COLUMN `task_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `sub_task` ADD CONSTRAINT `sub_task_task_id_fkey` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_task_id_fkey` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
