-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `comment_task_id_fkey`;

-- DropForeignKey
ALTER TABLE `sub_task` DROP FOREIGN KEY `sub_task_task_id_fkey`;

-- DropForeignKey
ALTER TABLE `task` DROP FOREIGN KEY `task_task_type_id_fkey`;

-- AddForeignKey
ALTER TABLE `task` ADD CONSTRAINT `task_task_type_id_fkey` FOREIGN KEY (`task_type_id`) REFERENCES `task_type`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `sub_task` ADD CONSTRAINT `sub_task_task_id_fkey` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_task_id_fkey` FOREIGN KEY (`task_id`) REFERENCES `task`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
