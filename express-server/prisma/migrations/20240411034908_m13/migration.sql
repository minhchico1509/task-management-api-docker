/*
  Warnings:

  - Added the required column `room_id` to the `award` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `award` ADD COLUMN `room_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `award` ADD CONSTRAINT `award_room_id_fkey` FOREIGN KEY (`room_id`) REFERENCES `room`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
