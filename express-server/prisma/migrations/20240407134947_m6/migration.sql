/*
  Warnings:

  - You are about to drop the `task_slot` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `due_date` to the `task` table without a default value. This is not possible if the table is not empty.
  - Added the required column `start_date` to the `task` table without a default value. This is not possible if the table is not empty.
  - Added the required column `status` to the `task` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `comment_task_slot_id_fkey`;

-- DropForeignKey
ALTER TABLE `sub_task` DROP FOREIGN KEY `sub_task_task_slot_id_fkey`;

-- DropForeignKey
ALTER TABLE `task_slot` DROP FOREIGN KEY `task_slot_task_id_fkey`;

-- AlterTable
ALTER TABLE `task` ADD COLUMN `due_date` DATETIME(3) NOT NULL,
    ADD COLUMN `start_date` DATETIME(3) NOT NULL,
    ADD COLUMN `status` ENUM('OPEN', 'IN_PROGRESS', 'CANCELLED', 'DONE') NOT NULL;

-- DropTable
DROP TABLE `task_slot`;

-- AddForeignKey
ALTER TABLE `sub_task` ADD CONSTRAINT `sub_task_task_slot_id_fkey` FOREIGN KEY (`task_slot_id`) REFERENCES `task`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_task_slot_id_fkey` FOREIGN KEY (`task_slot_id`) REFERENCES `task`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
