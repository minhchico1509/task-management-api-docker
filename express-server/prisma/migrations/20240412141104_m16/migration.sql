/*
  Warnings:

  - You are about to drop the column `member_id` on the `comment` table. All the data in the column will be lost.
  - Added the required column `commentator_id` to the `comment` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `comment` DROP FOREIGN KEY `comment_member_id_fkey`;

-- AlterTable
ALTER TABLE `comment` DROP COLUMN `member_id`,
    ADD COLUMN `commentator_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `comment` ADD CONSTRAINT `comment_commentator_id_fkey` FOREIGN KEY (`commentator_id`) REFERENCES `room_member`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
